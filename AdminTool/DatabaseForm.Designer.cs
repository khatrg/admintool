﻿namespace AdminTool
{
    partial class DatabaseForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DatabaseForm));
            this.miniToolStrip = new System.Windows.Forms.ToolStrip();
            this.ExcGB = new System.Windows.Forms.GroupBox();
            this.ExcDataGrid = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.From = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.To = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ExcToolStrip = new System.Windows.Forms.ToolStrip();
            this.DBCodeCbx = new System.Windows.Forms.ToolStripComboBox();
            this.ColDBCbx = new System.Windows.Forms.ToolStripComboBox();
            this.FromTbx = new System.Windows.Forms.ToolStripTextBox();
            this.ToTbx = new System.Windows.Forms.ToolStripTextBox();
            this.AddBtn = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.DBCbx2 = new System.Windows.Forms.ToolStripComboBox();
            this.CopyDbx = new System.Windows.Forms.ToolStripDropDownButton();
            this.copyExceptionsToDBOnlyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyExceptionsToAllDatabasesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.DelBtn = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.DBGrbx = new System.Windows.Forms.GroupBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.DBCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DBName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Active = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.BDToolStrip = new System.Windows.Forms.ToolStrip();
            this.ExportDBBtn = new System.Windows.Forms.ToolStripButton();
            this.RuleGrbx = new System.Windows.Forms.GroupBox();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.Rule = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ActiveRule = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.RulesToolStrip = new System.Windows.Forms.ToolStrip();
            this.ExportRuleBtn = new System.Windows.Forms.ToolStripButton();
            this.ExcGB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ExcDataGrid)).BeginInit();
            this.ExcToolStrip.SuspendLayout();
            this.DBGrbx.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.BDToolStrip.SuspendLayout();
            this.RuleGrbx.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.RulesToolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // miniToolStrip
            // 
            this.miniToolStrip.AutoSize = false;
            this.miniToolStrip.CanOverflow = false;
            this.miniToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.miniToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.miniToolStrip.Location = new System.Drawing.Point(9, 3);
            this.miniToolStrip.Name = "miniToolStrip";
            this.miniToolStrip.Size = new System.Drawing.Size(765, 25);
            this.miniToolStrip.TabIndex = 1;
            // 
            // ExcGB
            // 
            this.ExcGB.Controls.Add(this.ExcDataGrid);
            this.ExcGB.Controls.Add(this.ExcToolStrip);
            this.ExcGB.ForeColor = System.Drawing.Color.Blue;
            this.ExcGB.Location = new System.Drawing.Point(12, 298);
            this.ExcGB.Name = "ExcGB";
            this.ExcGB.Size = new System.Drawing.Size(1063, 283);
            this.ExcGB.TabIndex = 3;
            this.ExcGB.TabStop = false;
            this.ExcGB.Text = "Exceptions";
            // 
            // ExcDataGrid
            // 
            this.ExcDataGrid.AllowUserToAddRows = false;
            this.ExcDataGrid.AllowUserToDeleteRows = false;
            this.ExcDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ExcDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.From,
            this.To});
            this.ExcDataGrid.Location = new System.Drawing.Point(6, 44);
            this.ExcDataGrid.Name = "ExcDataGrid";
            this.ExcDataGrid.ReadOnly = true;
            this.ExcDataGrid.Size = new System.Drawing.Size(1051, 233);
            this.ExcDataGrid.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Rule";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 450;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Column";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 350;
            // 
            // From
            // 
            this.From.HeaderText = "From";
            this.From.Name = "From";
            this.From.ReadOnly = true;
            this.From.Width = 104;
            // 
            // To
            // 
            this.To.HeaderText = "To";
            this.To.Name = "To";
            this.To.ReadOnly = true;
            this.To.Width = 104;
            // 
            // ExcToolStrip
            // 
            this.ExcToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.DBCodeCbx,
            this.ColDBCbx,
            this.FromTbx,
            this.ToTbx,
            this.AddBtn,
            this.toolStripSeparator1,
            this.toolStripLabel1,
            this.DBCbx2,
            this.CopyDbx,
            this.toolStripSeparator2,
            this.DelBtn,
            this.toolStripButton1});
            this.ExcToolStrip.Location = new System.Drawing.Point(3, 16);
            this.ExcToolStrip.Name = "ExcToolStrip";
            this.ExcToolStrip.Size = new System.Drawing.Size(1057, 25);
            this.ExcToolStrip.TabIndex = 2;
            this.ExcToolStrip.Text = "ExcToolStrip";
            // 
            // DBCodeCbx
            // 
            this.DBCodeCbx.Margin = new System.Windows.Forms.Padding(1, 0, 5, 0);
            this.DBCodeCbx.Name = "DBCodeCbx";
            this.DBCodeCbx.Size = new System.Drawing.Size(200, 25);
            this.DBCodeCbx.ToolTipText = "Rules";
            // 
            // ColDBCbx
            // 
            this.ColDBCbx.Margin = new System.Windows.Forms.Padding(1, 0, 5, 0);
            this.ColDBCbx.Name = "ColDBCbx";
            this.ColDBCbx.Size = new System.Drawing.Size(200, 25);
            this.ColDBCbx.ToolTipText = "Column";
            this.ColDBCbx.Click += new System.EventHandler(this.ColDBCbx_Click);
            // 
            // FromTbx
            // 
            this.FromTbx.Margin = new System.Windows.Forms.Padding(1, 0, 5, 0);
            this.FromTbx.Name = "FromTbx";
            this.FromTbx.Size = new System.Drawing.Size(100, 25);
            this.FromTbx.ToolTipText = "From";
            // 
            // ToTbx
            // 
            this.ToTbx.AutoToolTip = true;
            this.ToTbx.Margin = new System.Windows.Forms.Padding(1, 0, 5, 0);
            this.ToTbx.Name = "ToTbx";
            this.ToTbx.Size = new System.Drawing.Size(100, 25);
            this.ToTbx.ToolTipText = "To";
            // 
            // AddBtn
            // 
            this.AddBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.AddBtn.Image = ((System.Drawing.Image)(resources.GetObject("AddBtn.Image")));
            this.AddBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.AddBtn.Margin = new System.Windows.Forms.Padding(0, 1, 5, 2);
            this.AddBtn.Name = "AddBtn";
            this.AddBtn.Size = new System.Drawing.Size(23, 22);
            this.AddBtn.Text = "Add";
            this.AddBtn.Click += new System.EventHandler(this.AddBtn_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Margin = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(69, 22);
            this.toolStripLabel1.Text = "Copy From:";
            // 
            // DBCbx2
            // 
            this.DBCbx2.Margin = new System.Windows.Forms.Padding(1, 0, 5, 0);
            this.DBCbx2.Name = "DBCbx2";
            this.DBCbx2.Size = new System.Drawing.Size(100, 25);
            this.DBCbx2.ToolTipText = "Database";
            // 
            // CopyDbx
            // 
            this.CopyDbx.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.CopyDbx.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copyExceptionsToDBOnlyToolStripMenuItem,
            this.copyExceptionsToAllDatabasesToolStripMenuItem});
            this.CopyDbx.Image = ((System.Drawing.Image)(resources.GetObject("CopyDbx.Image")));
            this.CopyDbx.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.CopyDbx.Margin = new System.Windows.Forms.Padding(0, 1, 5, 2);
            this.CopyDbx.Name = "CopyDbx";
            this.CopyDbx.Size = new System.Drawing.Size(29, 22);
            this.CopyDbx.Text = "Copy Exception";
            // 
            // copyExceptionsToDBOnlyToolStripMenuItem
            // 
            this.copyExceptionsToDBOnlyToolStripMenuItem.Name = "copyExceptionsToDBOnlyToolStripMenuItem";
            this.copyExceptionsToDBOnlyToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.copyExceptionsToDBOnlyToolStripMenuItem.Text = "Copy Exceptions to <DB> Only";
            // 
            // copyExceptionsToAllDatabasesToolStripMenuItem
            // 
            this.copyExceptionsToAllDatabasesToolStripMenuItem.Name = "copyExceptionsToAllDatabasesToolStripMenuItem";
            this.copyExceptionsToAllDatabasesToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.copyExceptionsToAllDatabasesToolStripMenuItem.Text = "Copy Exception to All Databases";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Margin = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // DelBtn
            // 
            this.DelBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.DelBtn.Image = ((System.Drawing.Image)(resources.GetObject("DelBtn.Image")));
            this.DelBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.DelBtn.Name = "DelBtn";
            this.DelBtn.Size = new System.Drawing.Size(23, 22);
            this.DelBtn.Text = "Delete";
            this.DelBtn.Click += new System.EventHandler(this.DelBtn_Click_1);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton1.Text = "Export To Excel";
            // 
            // DBGrbx
            // 
            this.DBGrbx.Controls.Add(this.dataGridView1);
            this.DBGrbx.Controls.Add(this.BDToolStrip);
            this.DBGrbx.ForeColor = System.Drawing.Color.Blue;
            this.DBGrbx.Location = new System.Drawing.Point(12, 12);
            this.DBGrbx.Name = "DBGrbx";
            this.DBGrbx.Size = new System.Drawing.Size(535, 280);
            this.DBGrbx.TabIndex = 4;
            this.DBGrbx.TabStop = false;
            this.DBGrbx.Text = "Databases";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DBCode,
            this.DBName,
            this.Active});
            this.dataGridView1.Location = new System.Drawing.Point(6, 44);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(523, 230);
            this.dataGridView1.TabIndex = 2;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // DBCode
            // 
            this.DBCode.HeaderText = "DB Code";
            this.DBCode.Name = "DBCode";
            // 
            // DBName
            // 
            this.DBName.HeaderText = "DB Name";
            this.DBName.Name = "DBName";
            this.DBName.Width = 280;
            // 
            // Active
            // 
            this.Active.HeaderText = "Active";
            this.Active.Name = "Active";
            this.Active.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Active.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // BDToolStrip
            // 
            this.BDToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ExportDBBtn});
            this.BDToolStrip.Location = new System.Drawing.Point(3, 16);
            this.BDToolStrip.Name = "BDToolStrip";
            this.BDToolStrip.Size = new System.Drawing.Size(529, 25);
            this.BDToolStrip.TabIndex = 1;
            this.BDToolStrip.Text = "toolStrip1";
            // 
            // ExportDBBtn
            // 
            this.ExportDBBtn.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.ExportDBBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ExportDBBtn.Image = ((System.Drawing.Image)(resources.GetObject("ExportDBBtn.Image")));
            this.ExportDBBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ExportDBBtn.Name = "ExportDBBtn";
            this.ExportDBBtn.Size = new System.Drawing.Size(23, 22);
            this.ExportDBBtn.Text = "Export To Excel";
            // 
            // RuleGrbx
            // 
            this.RuleGrbx.Controls.Add(this.dataGridView2);
            this.RuleGrbx.Controls.Add(this.RulesToolStrip);
            this.RuleGrbx.ForeColor = System.Drawing.Color.Blue;
            this.RuleGrbx.Location = new System.Drawing.Point(553, 12);
            this.RuleGrbx.Name = "RuleGrbx";
            this.RuleGrbx.Size = new System.Drawing.Size(522, 280);
            this.RuleGrbx.TabIndex = 5;
            this.RuleGrbx.TabStop = false;
            this.RuleGrbx.Text = "Rules";
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Rule,
            this.ActiveRule});
            this.dataGridView2.Location = new System.Drawing.Point(6, 44);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(510, 230);
            this.dataGridView2.TabIndex = 3;
            // 
            // Rule
            // 
            this.Rule.HeaderText = "Rule";
            this.Rule.Name = "Rule";
            this.Rule.Width = 367;
            // 
            // ActiveRule
            // 
            this.ActiveRule.HeaderText = "Active";
            this.ActiveRule.Name = "ActiveRule";
            this.ActiveRule.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ActiveRule.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // RulesToolStrip
            // 
            this.RulesToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ExportRuleBtn});
            this.RulesToolStrip.Location = new System.Drawing.Point(3, 16);
            this.RulesToolStrip.Name = "RulesToolStrip";
            this.RulesToolStrip.Size = new System.Drawing.Size(516, 25);
            this.RulesToolStrip.TabIndex = 1;
            this.RulesToolStrip.Text = "toolStrip2";
            // 
            // ExportRuleBtn
            // 
            this.ExportRuleBtn.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.ExportRuleBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ExportRuleBtn.Image = ((System.Drawing.Image)(resources.GetObject("ExportRuleBtn.Image")));
            this.ExportRuleBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ExportRuleBtn.Name = "ExportRuleBtn";
            this.ExportRuleBtn.Size = new System.Drawing.Size(23, 22);
            this.ExportRuleBtn.Text = "Export To Excel";
            // 
            // DatabaseForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1087, 581);
            this.Controls.Add(this.RuleGrbx);
            this.Controls.Add(this.DBGrbx);
            this.Controls.Add(this.ExcGB);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "DatabaseForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Database Management";
            this.ExcGB.ResumeLayout(false);
            this.ExcGB.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ExcDataGrid)).EndInit();
            this.ExcToolStrip.ResumeLayout(false);
            this.ExcToolStrip.PerformLayout();
            this.DBGrbx.ResumeLayout(false);
            this.DBGrbx.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.BDToolStrip.ResumeLayout(false);
            this.BDToolStrip.PerformLayout();
            this.RuleGrbx.ResumeLayout(false);
            this.RuleGrbx.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.RulesToolStrip.ResumeLayout(false);
            this.RulesToolStrip.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStrip miniToolStrip;
        private System.Windows.Forms.GroupBox ExcGB;
        private System.Windows.Forms.DataGridView ExcDataGrid;
        private System.Windows.Forms.ToolStrip ExcToolStrip;
        private System.Windows.Forms.ToolStripComboBox DBCodeCbx;
        private System.Windows.Forms.ToolStripComboBox ColDBCbx;
        private System.Windows.Forms.ToolStripTextBox FromTbx;
        private System.Windows.Forms.ToolStripTextBox ToTbx;
        private System.Windows.Forms.ToolStripButton AddBtn;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripComboBox DBCbx2;
        private System.Windows.Forms.ToolStripDropDownButton CopyDbx;
        private System.Windows.Forms.ToolStripMenuItem copyExceptionsToDBOnlyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyExceptionsToAllDatabasesToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton DelBtn;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.GroupBox DBGrbx;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ToolStrip BDToolStrip;
        private System.Windows.Forms.ToolStripButton ExportDBBtn;
        private System.Windows.Forms.GroupBox RuleGrbx;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.ToolStrip RulesToolStrip;
        private System.Windows.Forms.ToolStripButton ExportRuleBtn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn From;
        private System.Windows.Forms.DataGridViewTextBoxColumn To;
        private System.Windows.Forms.DataGridViewTextBoxColumn DBCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn DBName;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Active;
        private System.Windows.Forms.DataGridViewTextBoxColumn Rule;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ActiveRule;
    }
}