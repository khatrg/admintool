﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminTool.CommonService
{
    public static class RuleManagementServices
    {
        static string MasterConnectionString = CommonService.SetupLoginServices.MasterConnectionString;
        static string AdminConnectionString = Properties.Resources.ConnectionString_AdminDB;
        public static List<KeyValuePair<string, string>> getRuleList()
        {
            List<KeyValuePair<string, string>> RuleList = new List<KeyValuePair<string, string>>();
            try
            {
                using (SqlConnection DBConnection = new SqlConnection(AdminConnectionString))
                {
                    DBConnection.Open();
                    string sqlGet = @"select [RULE_CODE], [DESCR] from dbo.[RULE_MANAGE] order by [DESCR] DESC";
                    using (SqlCommand SVPackCommand = new SqlCommand(sqlGet, DBConnection))
                    {
                        using (SqlDataReader Reader = SVPackCommand.ExecuteReader())
                        {
                            while (Reader.Read())
                            {
                                RuleList.Insert(0, new KeyValuePair<string, string>(Reader["RULE_CODE"].ToString(), Reader["DESCR"].ToString()));
                            }
                        }
                    }
                    DBConnection.Close();
                }
            }
            catch (SqlException ex)
            {
                CommonService.SetupLoginServices.Logger(ex.ToString());
            }
            return RuleList;
        }

        public static List<KeyValuePair<string, string>> getColumnRuleList(string SunTableKey)
        {
            List<KeyValuePair<string,string>> ColumnList = new List<KeyValuePair<string, string>>();
            try
            {
                using (SqlConnection DBConnection = new SqlConnection(AdminConnectionString))
                {
                    DBConnection.Open();
                    string sqlGet = @"select [COLUMN_CODE],[DESCR] from dbo.[COLUMN_RULE_MAPPING] where [RULE_CODE] = '" + SunTableKey + "' and IS_ACTIVE=1 order by ID DESC";
                    using (SqlCommand SVPackCommand = new SqlCommand(sqlGet, DBConnection))
                    {
                        using (SqlDataReader Reader = SVPackCommand.ExecuteReader())
                        {
                            while (Reader.Read())
                            {
                                ColumnList.Insert(0, new KeyValuePair<string, string>(Reader["COLUMN_CODE"].ToString(), Reader["DESCR"].ToString()));
                            }
                        }
                    }
                    DBConnection.Close();
                }
            }
            catch (SqlException ex)
            {
                CommonService.SetupLoginServices.Logger(ex.ToString());
            }
            return ColumnList;
        }
        public static List<KeyValuePair<string, string>> getColumnForExceptionList(string SunTableKey)
        {
            List<KeyValuePair<string, string>> ColumnList = new List<KeyValuePair<string, string>>();
            try
            {
                using (SqlConnection DBConnection = new SqlConnection(AdminConnectionString))
                {
                    DBConnection.Open();
                    string sqlGet = @"select [COLUMN_CODE],[DESCR] from dbo.[COLUMN_RULE_MAPPING] where [RULE_CODE] = '" + SunTableKey + "' order by ID DESC";
                    using (SqlCommand SVPackCommand = new SqlCommand(sqlGet, DBConnection))
                    {
                        using (SqlDataReader Reader = SVPackCommand.ExecuteReader())
                        {
                            while (Reader.Read())
                            {
                                ColumnList.Insert(0, new KeyValuePair<string, string>(Reader["COLUMN_CODE"].ToString(), Reader["DESCR"].ToString()));
                            }
                        }
                    }
                    DBConnection.Close();
                }
            }
            catch (SqlException ex)
            {
                CommonService.SetupLoginServices.Logger(ex.ToString());
            }
            return ColumnList;
        }

        public static List<KeyValuePair<string, string>> getActiveDBForRule(string RuleCode)
        {
            List<KeyValuePair<string, string>> ColumnList = new List<KeyValuePair<string, string>>();
            try
            {
                using (SqlConnection DBConnection = new SqlConnection(AdminConnectionString))
                {
                    DBConnection.Open();
                    string sqlGet = @"select BU_CODE, IS_ENABLE from BU_RULE_MANAGE where RULE_CODE = '" + RuleCode + "'";
                    using (SqlCommand SVPackCommand = new SqlCommand(sqlGet, DBConnection))
                    {
                        using (SqlDataReader Reader = SVPackCommand.ExecuteReader())
                        {
                            while (Reader.Read())
                            {
                                ColumnList.Insert(0, new KeyValuePair<string, string>(Reader["BU_CODE"].ToString(), Reader["IS_ENABLE"].ToString()));
                            }
                        }
                    }
                    DBConnection.Close();
                }
            }
            catch (SqlException ex)
            {
                CommonService.SetupLoginServices.Logger(ex.ToString());
            }
            return ColumnList;
        }
    }
}
