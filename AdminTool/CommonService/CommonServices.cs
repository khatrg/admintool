﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminTool.CommonService
{
    public static class CommonServices
    {
        public static DataTable getActiveBURules()
        {
            DataTable BURulesTable = new DataTable();
            try
            {
                string sqlGetData = @"select mr.BU_CODE, r.RULE_CODE, r.DESCR, mr.IS_ENABLE
                                        from BU_RULE_MANAGE mr, RULE_MANAGE r
                                        where mr.RULE_CODE = r.RULE_CODE";
                using (SqlConnection DBConnection = new SqlConnection(Properties.Resources.ConnectionString_AdminDB))
                {
                    DBConnection.Open();
                    using (SqlCommand Command = new SqlCommand(sqlGetData, DBConnection))
                    {
                        SqlDataAdapter da = new SqlDataAdapter(Command);
                        da.Fill(BURulesTable);
                        da.Dispose();
                    }
                    DBConnection.Close();
                }
            }
            catch (SqlException ex)
            {
                CommonService.SetupLoginServices.Logger(ex.ToString());
            }
            return BURulesTable;
        }

        public static DataTable getActiveRulesForBU(string BUCode)
        {
            DataTable BURulesTable = new DataTable();
            try
            {
                string sqlGetData = @"select r.DESCR, mr.IS_ENABLE
                                    from BU_RULE_MANAGE mr, RULE_MANAGE r
                                    where mr.RULE_CODE = r.RULE_CODE and mr.BU_CODE='" + BUCode + "'";
                using (SqlConnection DBConnection = new SqlConnection(Properties.Resources.ConnectionString_AdminDB))
                {
                    DBConnection.Open();
                    using (SqlCommand Command = new SqlCommand(sqlGetData, DBConnection))
                    {
                        SqlDataAdapter da = new SqlDataAdapter(Command);
                        da.Fill(BURulesTable);
                        da.Dispose();
                    }
                    DBConnection.Close();
                }
            }
            catch (SqlException ex)
            {
                CommonService.SetupLoginServices.Logger(ex.ToString());
            }
            return BURulesTable;
        }

        public static DataTable getExceptionsForBU(string BUCode)
        {
            DataTable ExceptionTable = new DataTable();
            try
            {
                string sqlGetData = @"select * from EXCEPTION where BU_CODE ='" + BUCode + "'";
                using (SqlConnection DBConnection = new SqlConnection(Properties.Resources.ConnectionString_AdminDB))
                {
                    DBConnection.Open();
                    using (SqlCommand Command = new SqlCommand(sqlGetData, DBConnection))
                    {
                        SqlDataAdapter da = new SqlDataAdapter(Command);
                        da.Fill(ExceptionTable);
                        da.Dispose();
                    }
                    DBConnection.Close();
                }
            }
            catch (SqlException ex)
            {
                CommonService.SetupLoginServices.Logger(ex.ToString());
            }
            return ExceptionTable;
        }

        public static DataTable getExceptionsForGridView(string BUCode, string RuleCode)
        {
            DataTable ExceptionTable = new DataTable();
            try
            {
                string sqlGetData = @"select * from EXCEPTION where BU_CODE ='" + BUCode + "' and RULE_CODE = '" + RuleCode + "'";
                using (SqlConnection DBConnection = new SqlConnection(Properties.Resources.ConnectionString_AdminDB))
                {
                    DBConnection.Open();
                    using (SqlCommand Command = new SqlCommand(sqlGetData, DBConnection))
                    {
                        SqlDataAdapter da = new SqlDataAdapter(Command);
                        da.Fill(ExceptionTable);
                        da.Dispose();
                    }
                    DBConnection.Close();
                }
            }
            catch (SqlException ex)
            {
                CommonService.SetupLoginServices.Logger(ex.ToString());
            }
            return ExceptionTable;
        }

        public static bool ExportBURulesActive(DataTable BURulesTable, DataTable DBCodeTable, List<KeyValuePair<string, string>> RulesList)
        {
            bool result = false;
            DataTable RulesTableToExport = new DataTable();
            RulesTableToExport.Columns.Add("RULES");
            try
            {
                foreach (DataRow DBDtr in DBCodeTable.Rows)
                {
                    RulesTableToExport.Columns.Add(DBDtr["BU_CODE"].ToString());
                }
                foreach (KeyValuePair<string, string> rule in RulesList)
                {
                    DataRow newRow = RulesTableToExport.NewRow();
                    foreach (DataRow DBDtr in DBCodeTable.Rows)
                    {
                        newRow["RULES"] = rule.Value.ToString();
                        DataTable filterTable = BURulesTable.AsEnumerable().Where(row => row.Field<string>("BU_CODE").Trim() == DBDtr["BU_CODE"].ToString().Trim() && row.Field<String>("RULE_CODE").Trim() == rule.Key.ToString().Trim()).CopyToDataTable();
                        if ((bool)filterTable.Rows[0]["IS_ENABLE"])
                        {
                            newRow[DBDtr["BU_CODE"].ToString()] = "Y";
                        }
                        else
                        {
                            newRow[DBDtr["BU_CODE"].ToString()] = "N";
                        }
                    }
                    RulesTableToExport.Rows.Add(newRow);
                }
                ExportToExcel(RulesTableToExport, "RulesBUActive", "Rules-BU");
                result = true;
            }
            catch (Exception ex)
            {
                CommonService.SetupLoginServices.Logger(ex.ToString());
                result = false;
            }
            return result;
        }

        public static bool ExportRulesActive(DataTable RulesActiveTable, string BUCode)
        {
            bool result = false;
            DataTable TableToExport = new DataTable();
            TableToExport.Columns.Add("RULE");
            TableToExport.Columns.Add("ACTIVE");
            try
            {
                foreach (DataRow Dtr in RulesActiveTable.Rows)
                {
                    DataRow newRow = TableToExport.NewRow();
                    newRow["RULE"] = Dtr["DESCR"].ToString();
                    if ((bool)Dtr["IS_ENABLE"])
                    {
                        newRow["ACTIVE"] = "True";
                    }
                    else
                    {
                        newRow["ACTIVE"] = "False";
                    }
                    TableToExport.Rows.Add(newRow);
                }
                ExportToExcel(TableToExport, "RulesActive", "Rules for " + BUCode);
                result = true;
            }
            catch (Exception ex)
            {
                CommonService.SetupLoginServices.Logger(ex.ToString());
                result = false;
            }
            return result;
        }

        public static DataTable getFilterException(string BUCode, string RuleCode, string ColumnCode)
        {
            DataTable BURulesTable = new DataTable();
            try
            {
                string sqlGetData = @"select r.DESCR as RULEDESCR, cr.DESCR as COLDESCR, ex.[FROM], ex.[TO]
                                        from EXCEPTION ex join RULE_MANAGE r on ex.RULE_CODE = r.RULE_CODE 
		                                        join COLUMN_RULE_MAPPING cr on ex.COLUMN_CODE = cr.COLUMN_CODE
                                        where ex.BU_CODE = '" + BUCode + "' and ex.COLUMN_CODE = '" + ColumnCode + "'and cr.RULE_CODE = '"+ RuleCode + "'";
                using (SqlConnection DBConnection = new SqlConnection(Properties.Resources.ConnectionString_AdminDB))
                {
                    DBConnection.Open();
                    using (SqlCommand Command = new SqlCommand(sqlGetData, DBConnection))
                    {
                        SqlDataAdapter da = new SqlDataAdapter(Command);
                        da.Fill(BURulesTable);
                        da.Dispose();
                    }
                    DBConnection.Close();
                }
            }
            catch (SqlException ex)
            {
                CommonService.SetupLoginServices.Logger(ex.ToString());
            }
            return BURulesTable;
        }
        public static bool ExportExceptions(DataTable ExceptionTable, string BUCode)
        {
            bool result = false;
            DataTable TableToExport = new DataTable();
            TableToExport.Columns.Add("RULE");
            TableToExport.Columns.Add("COLUMN");
            TableToExport.Columns.Add("FROM");
            TableToExport.Columns.Add("TO");
            try
            {
                foreach (DataRow Dtr in ExceptionTable.Rows)
                {
                    DataTable FilterTable = getFilterException(BUCode, Dtr["RULE_CODE"].ToString(), Dtr["COLUMN_CODE"].ToString());
                    foreach(DataRow DtrFilter in FilterTable.Rows)
                    {
                        DataRow newRow = TableToExport.NewRow();
                        newRow["RULE"] = DtrFilter["RULEDESCR"].ToString();
                        newRow["COLUMN"] = DtrFilter["COLDESCR"].ToString();
                        newRow["FROM"] = DtrFilter["FROM"].ToString();
                        newRow["TO"] = DtrFilter["TO"].ToString();
                        TableToExport.Rows.Add(newRow);
                    }
                }
                ExportToExcel(TableToExport, "ExceptionBU", "Exceptions for " + BUCode);
                result = true;
            }
            catch (Exception ex)
            {
                CommonService.SetupLoginServices.Logger(ex.ToString());
                result = false;
            }
            return result;
        }

        static void ExportToExcel(DataTable data, string Filename, string SheetDescr)
        {
            string folderPath = Properties.Resources.LoggerDirectory;
            Filename = Filename + DateTime.Now.Millisecond.ToString();
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(data, SheetDescr);
                //using (var ws = wb.Worksheets.Add(data, "Rules"))
                //{
                //    ws.Cell("A1").Value = 42;
                //    ws.Cell("A1").AddConditionalFormat().WhenLessThan(100)
                //        .Fill.SetBackgroundColor(XLColor.Red);
                //}
                wb.SaveAs(folderPath + Filename + ".xlsx");
            }
        }
    }
}
