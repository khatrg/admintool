﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AdminTool.CommonService
{
    public static class SetupLoginServices
    {
        public static bool CheckMasterConnectionString(string ConnectionStr)
        {
            bool result = false;
            try
            {
                using (SqlConnection DBConnection = new SqlConnection(ConnectionStr))
                {
                    DBConnection.Open();
                    string sqlCheck = @"select top 1 CONNECTION_STRING from MASTER_PROPERTY";
                    using (SqlCommand SVPackCommand = new SqlCommand(sqlCheck, DBConnection))
                    {
                        using (SqlDataReader Reader = SVPackCommand.ExecuteReader())
                        {
                            while (Reader.Read())
                            {
                                if (Reader["CONNECTION_STRING"].ToString() != "" && Reader["CONNECTION_STRING"].ToString() != null)
                                {
                                    result = true;
                                    MasterConnectionString = Reader["CONNECTION_STRING"].ToString();
                                    MasterConnectionString = DecryptString(MasterConnectionString, initVector);
                                }
                                else result = false;
                            }
                        }
                    }
                    DBConnection.Close();
                }
            }
            catch (SqlException ex)
            {
                Logger(ex.ToString());
                result = false;
            }
            return result;
        }
        public static string MasterConnectionString;
        public static bool CheckMasterDB(string ConnectionStr)
        {
            bool result = false;
            try
            {
                using (SqlConnection DBConnection = new SqlConnection(ConnectionStr))
                {
                    DBConnection.Open();
                    string sqlCheck = @"select top 1 DB_CODE from MASTER_PROPERTY";
                    using (SqlCommand SVPackCommand = new SqlCommand(sqlCheck, DBConnection))
                    {
                        using (SqlDataReader Reader = SVPackCommand.ExecuteReader())
                        {
                            while (Reader.Read())
                            {
                                if (Reader["DB_CODE"].ToString().Trim() != "" && Reader["DB_CODE"].ToString() != null)
                                {
                                    result = true;
                                }
                                else result = false;
                            }
                        }
                    }
                    DBConnection.Close();
                }
            }
            catch (SqlException ex)
            {
                Logger(ex.ToString());
                result = false;
            }
            return result;
        }

        public static DataTable getDBCodeList()
        {
            DataTable DBCodeTable = new DataTable();
            try
            {
<<<<<<< HEAD
                string sqlGetData = @"select BU_CODE, DESCR from [SunSystemsDomain].[dbo].[DOMN_BU_DSRCE_LINK]";
                //string MasterConnectionStrings = DecryptString(MasterConnectionString, initVector);
=======
                string sqlGetData = @"select BU_CODE, DESCR from [SunSystemsDomain].[dbo].[DOMN_BU_DSRCE_LINK] order by BU_CODE ASC";
                //MasterConnectionString = DecryptString(MasterConnectionString, initVector);
>>>>>>> kha

                using (SqlConnection DBConnection = new SqlConnection(MasterConnectionString))
                {
                    DBConnection.Open();
                    using (SqlCommand Command = new SqlCommand(sqlGetData, DBConnection))
                    {
                        SqlDataAdapter da = new SqlDataAdapter(Command);
                        da.Fill(DBCodeTable);
                        da.Dispose();
                    }
                    DBConnection.Close();
                }


            }
            catch (SqlException ex)
            {
                Logger(ex.ToString());
            }
            return DBCodeTable;
        }

        //Data Management(Rule)

        public static DataTable getRuleCodeList()
        {
            DataTable ruleCodeTable = new DataTable();
            try
            {
                string sqlGetRuleList = @"select DESCR, [RULE_CODE] from [AdminToolDatabase].[dbo].[RULE_MANAGE]";
                //string MasterConnectionStrings = DecryptString(MasterConnectionString, initVector);


                using (SqlConnection DBConnection = new SqlConnection(Properties.Resources.ConnectionString_AdminDB))
                {
                    DBConnection.Open();
                    using (SqlCommand Command = new SqlCommand(sqlGetRuleList, DBConnection))
                    {
                        SqlDataAdapter da = new SqlDataAdapter(Command);
                        da.Fill(ruleCodeTable);
                        da.Dispose();
                    }
                    DBConnection.Close();
                }

            }
            catch (SqlException ex)
            {
                Logger(ex.ToString());
            }
            return ruleCodeTable;
        }


        //Data Exception (Rule)

        public static DataTable getRuleExceptionList()
        {
            DataTable ruleCodeExcep = new DataTable();
            try
            {
                string sqlGetRuleList = @"select DESCR, [RULE_CODE] from [AdminToolDatabase].[dbo].[RULE_MANAGE]";
                //string MasterConnectionStrings = DecryptString(MasterConnectionString, initVector);


                using (SqlConnection DBConnection = new SqlConnection(Properties.Resources.ConnectionString_AdminDB))
                {
                    DBConnection.Open();
                    using (SqlCommand Command = new SqlCommand(sqlGetRuleList, DBConnection))
                    {
                        SqlDataAdapter da = new SqlDataAdapter(Command);
                        da.Fill(ruleCodeExcep);
                        da.Dispose();
                    }
                    DBConnection.Close();
                }

            }
            catch (SqlException ex)
            {
                Logger(ex.ToString());
            }
            return ruleCodeExcep;
        }

        //Exception Column
        public static DataTable getRuleExceptionColList(string currentData)
        {
            DataTable ruleColExcep = new DataTable();
            try
            {
                string sqlGetRuleColList = @"  select [COLUMN_RULE_MAPPING].COLUMN_CODE, [COLUMN_RULE_MAPPING].DESCR "
                                + " from [RULE_MANAGE] inner join [COLUMN_RULE_MAPPING] on "
                                + " REPLACE(REPLACE([RULE_MANAGE].RULE_CODE, CHAR(13), ''), CHAR(10), '')  = RTRIM([COLUMN_RULE_MAPPING].RULE_CODE)"
                                + " where REPLACE(REPLACE([RULE_MANAGE].RULE_CODE, CHAR(13), ''), CHAR(10), '') = '" + currentData + "'";
                //string MasterConnectionStrings = DecryptString(MasterConnectionString, initVector);


                using (SqlConnection DBConnection = new SqlConnection(Properties.Resources.ConnectionString_AdminDB))
                {
                    DBConnection.Open();
                    using (SqlCommand Command = new SqlCommand(sqlGetRuleColList, DBConnection))
                    {
                        SqlDataAdapter da = new SqlDataAdapter(Command);
                        da.Fill(ruleColExcep);
                        da.Dispose();
                    }
                    DBConnection.Close();
                }

            }
            catch (SqlException ex)
            {
                Logger(ex.ToString());
            }
            return ruleColExcep;
        }

        
        public static void SaveMasterConnectionString(string ConnectionStr)
        {
            try
            {
                ConnectionStr = EncryptString(ConnectionStr, initVector);
                using (SqlConnection DBConnection = new SqlConnection(Properties.Resources.ConnectionString_AdminDB))
                {
                    DBConnection.Open();
                    string sqlUpdate = @"UPDATE AdminToolDatabase.dbo.MASTER_PROPERTY SET CONNECTION_STRING = '" + ConnectionStr + "' WHERE [KEY]='TRGADMIN'";
                    using (SqlCommand SVPackCommand = new SqlCommand(sqlUpdate, DBConnection))
                    {
                        SVPackCommand.ExecuteNonQuery();
                    }
                    DBConnection.Close();
                }
                MasterConnectionString = ConnectionStr;
            }
            catch (SqlException ex)
            {
                Logger(ex.ToString());
            }
        }

        public static void SaveMasterDBCode(string DBCode)
        {
            try
            {
                string sqlUpdate = @"UPDATE MASTER_PROPERTY SET DB_CODE = '" + DBCode + "' WHERE [KEY]='TRGADMIN'";
                using (SqlConnection DBConnection = new SqlConnection(Properties.Resources.ConnectionString_AdminDB))
                {
                    DBConnection.Open();
                    using (SqlCommand SVPackCommand = new SqlCommand(sqlUpdate, DBConnection))
                    {
                        SVPackCommand.ExecuteNonQuery();
                    }
                    DBConnection.Close();
                }
            }
            catch (SqlException ex)
            {
                Logger(ex.ToString());
            }
        }

        public static bool IsServerConnected(string connectionString)
        {
            bool result = false;
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open(); // throws if invalid
                    //conn.State = ConnectionState.Open;
                    result = true;
                }
            }
            catch (SqlException)
            {
                result = false;
            }
            return result;
        }

        private const string initVector = "trgadministrator";
        private const int keysize = 256;
        public static string EncryptString(string plainText, string passPhrase)
        {
            byte[] initVectorBytes = Encoding.UTF8.GetBytes(initVector);
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, null);
            byte[] keyBytes = password.GetBytes(keysize / 8);
            RijndaelManaged symmetricKey = new RijndaelManaged();
            symmetricKey.Mode = CipherMode.CBC;
            ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes);
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
            cryptoStream.FlushFinalBlock();
            byte[] cipherTextBytes = memoryStream.ToArray();
            memoryStream.Close();
            cryptoStream.Close();
            return Convert.ToBase64String(cipherTextBytes);
        }
        //Decrypt
        public static string DecryptString(string cipherText, string passPhrase)
        {
            byte[] initVectorBytes = Encoding.UTF8.GetBytes(initVector);
            byte[] cipherTextBytes = Convert.FromBase64String(cipherText);
            PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, null);
            byte[] keyBytes = password.GetBytes(keysize / 8);
            RijndaelManaged symmetricKey = new RijndaelManaged();
            symmetricKey.Mode = CipherMode.CBC;
            ICryptoTransform decryptor = symmetricKey.CreateDecryptor(keyBytes, initVectorBytes);
            MemoryStream memoryStream = new MemoryStream(cipherTextBytes);
            CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
            byte[] plainTextBytes = new byte[cipherTextBytes.Length];
            int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
            memoryStream.Close();
            cryptoStream.Close();
            return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
        }

        public static void Logger(String lines)
        {
            string path = Properties.Resources.LoggerDirectory;
            string filename = "Log" + DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString()
                            + "-" + DateTime.Now.Day.ToString() + "-" + DateTime.Now.Millisecond.ToString() + ".txt";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
                using (StreamWriter sw = File.CreateText(path + filename))
                {
                    sw.WriteLine(lines);
                }
            }
            else
            {
                using (StreamWriter sw = File.CreateText(path + filename))
                {
                    sw.WriteLine(lines);
                }
            }
        }
    }
}
