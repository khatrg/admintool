﻿namespace AdminTool
{
    partial class SerializationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SerializationForm));
            this.GbxSerialization = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtSerialCode = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.DtpkExpireDate = new System.Windows.Forms.DateTimePicker();
            this.DtpkInstallDate = new System.Windows.Forms.DateTimePicker();
            this.TxtLisenceCount = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtAddress = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtEndUser = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.BtnLogin = new System.Windows.Forms.Button();
            this.BtnCancel = new System.Windows.Forms.Button();
            this.GbxSerialization.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // GbxSerialization
            // 
            this.GbxSerialization.Controls.Add(this.groupBox1);
            this.GbxSerialization.Controls.Add(this.TxtSerialCode);
            this.GbxSerialization.Controls.Add(this.label6);
            this.GbxSerialization.Controls.Add(this.DtpkExpireDate);
            this.GbxSerialization.Controls.Add(this.DtpkInstallDate);
            this.GbxSerialization.Controls.Add(this.TxtLisenceCount);
            this.GbxSerialization.Controls.Add(this.label5);
            this.GbxSerialization.Controls.Add(this.label4);
            this.GbxSerialization.Controls.Add(this.label3);
            this.GbxSerialization.Controls.Add(this.TxtAddress);
            this.GbxSerialization.Controls.Add(this.label2);
            this.GbxSerialization.Controls.Add(this.TxtEndUser);
            this.GbxSerialization.Controls.Add(this.label1);
            this.GbxSerialization.Controls.Add(this.pictureBox1);
            this.GbxSerialization.ForeColor = System.Drawing.Color.Blue;
            this.GbxSerialization.Location = new System.Drawing.Point(12, 12);
            this.GbxSerialization.Name = "GbxSerialization";
            this.GbxSerialization.Size = new System.Drawing.Size(653, 318);
            this.GbxSerialization.TabIndex = 0;
            this.GbxSerialization.TabStop = false;
            this.GbxSerialization.Text = "Serialization Information";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBox4);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Location = new System.Drawing.Point(78, 240);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(549, 60);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Serialization Expiry Warning Message";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(250, 23);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(64, 20);
            this.textBox4.TabIndex = 17;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label7.Location = new System.Drawing.Point(315, 26);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "days";
            // 
            // TxtSerialCode
            // 
            this.TxtSerialCode.Location = new System.Drawing.Point(326, 205);
            this.TxtSerialCode.Name = "TxtSerialCode";
            this.TxtSerialCode.Size = new System.Drawing.Size(206, 20);
            this.TxtSerialCode.TabIndex = 14;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label6.Location = new System.Drawing.Point(231, 208);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(91, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Serialization Code";
            // 
            // DtpkExpireDate
            // 
            this.DtpkExpireDate.Location = new System.Drawing.Point(326, 136);
            this.DtpkExpireDate.Name = "DtpkExpireDate";
            this.DtpkExpireDate.Size = new System.Drawing.Size(206, 20);
            this.DtpkExpireDate.TabIndex = 12;
            // 
            // DtpkInstallDate
            // 
            this.DtpkInstallDate.Location = new System.Drawing.Point(326, 101);
            this.DtpkInstallDate.Name = "DtpkInstallDate";
            this.DtpkInstallDate.Size = new System.Drawing.Size(206, 20);
            this.DtpkInstallDate.TabIndex = 11;
            // 
            // TxtLisenceCount
            // 
            this.TxtLisenceCount.Location = new System.Drawing.Point(326, 171);
            this.TxtLisenceCount.Name = "TxtLisenceCount";
            this.TxtLisenceCount.Size = new System.Drawing.Size(64, 20);
            this.TxtLisenceCount.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label5.Location = new System.Drawing.Point(247, 174);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Lisence Count";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label4.Location = new System.Drawing.Point(260, 142);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Expiry Date";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label3.Location = new System.Drawing.Point(260, 106);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Install Date";
            // 
            // TxtAddress
            // 
            this.TxtAddress.Location = new System.Drawing.Point(326, 68);
            this.TxtAddress.Name = "TxtAddress";
            this.TxtAddress.Size = new System.Drawing.Size(291, 20);
            this.TxtAddress.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label2.Location = new System.Drawing.Point(275, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Address";
            // 
            // TxtEndUser
            // 
            this.TxtEndUser.Location = new System.Drawing.Point(326, 33);
            this.TxtEndUser.Name = "TxtEndUser";
            this.TxtEndUser.Size = new System.Drawing.Size(291, 20);
            this.TxtEndUser.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label1.Location = new System.Drawing.Point(269, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "End-User";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(22, 36);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(170, 170);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // BtnLogin
            // 
            this.BtnLogin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(126)))), ((int)(((byte)(142)))));
            this.BtnLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnLogin.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.BtnLogin.Location = new System.Drawing.Point(195, 345);
            this.BtnLogin.Name = "BtnLogin";
            this.BtnLogin.Size = new System.Drawing.Size(153, 35);
            this.BtnLogin.TabIndex = 23;
            this.BtnLogin.Text = "Save Changes";
            this.BtnLogin.UseVisualStyleBackColor = false;
            // 
            // BtnCancel
            // 
            this.BtnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Location = new System.Drawing.Point(373, 345);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(140, 35);
            this.BtnCancel.TabIndex = 22;
            this.BtnCancel.Text = "Cancel";
            this.BtnCancel.UseVisualStyleBackColor = true;
            // 
            // SerializationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(677, 392);
            this.Controls.Add(this.BtnLogin);
            this.Controls.Add(this.BtnCancel);
            this.Controls.Add(this.GbxSerialization);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "SerializationForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Serialization";
            this.GbxSerialization.ResumeLayout(false);
            this.GbxSerialization.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GbxSerialization;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox TxtSerialCode;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker DtpkExpireDate;
        private System.Windows.Forms.DateTimePicker DtpkInstallDate;
        private System.Windows.Forms.TextBox TxtLisenceCount;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TxtAddress;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TxtEndUser;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button BtnLogin;
        private System.Windows.Forms.Button BtnCancel;
    }
}