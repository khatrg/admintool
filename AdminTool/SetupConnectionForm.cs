﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AdminTool
{
    public partial class SetupConnectionForm : Form
    {
        public SetupConnectionForm()
        {
            InitializeComponent();
        }
        string connectionString = "";
        void BtnTest_Click(object sender, EventArgs e)
        {
            BtnTest.Enabled = false;
            BtnNext.Enabled = false;
            BtnCancel.Enabled = false;
            CheckConnection();
        }

        void BtnNext_Click(object sender, EventArgs e)
        {
            if (CheckConnection())
            {
                CommonService.SetupLoginServices.SaveMasterConnectionString(connectionString);
                bool MasterDBFlag = CommonService.SetupLoginServices.CheckMasterDB(connectionString);
                if (MasterDBFlag)
                {
                    MainForm mf = new MainForm();
                    mf.Show();
                    this.Hide();
                }
                else
                {
                    MainDBList mdb = new MainDBList();
                    mdb.Show();
                    this.Hide();
                }
            }
        }
        
        bool CheckConnection()
        {
            bool result = false;
            string serverName = TxtServerName.Text;
            string databaseName = TxtDBName.Text;
            string username = TxtUsername.Text;
            string password = TxtPassword.Text;
            string warningMsg = "";
            if (serverName == "" || databaseName == "")
            {
                warningMsg = Properties.Resources.ConnectionString_ValidServer;
                NoticeForm nf = new NoticeForm(warningMsg, Color.Red);
                nf.Show();
            }
            else
            {
                if (username == "" && password == "")
                {
                    connectionString = "Server=" + serverName + ";" + "Initial Catalog=" + databaseName + ";" + "Integrated Security=true";
                }
                else if (username != "" && password == "")
                {
                    warningMsg = Properties.Resources.ConnectionString_UsernamePasswordPair_Warning;
                    NoticeForm nf = new NoticeForm(warningMsg, Color.Red);
                    nf.Show();
                }
                else if (username == "" && password != "")
                {
                    warningMsg = Properties.Resources.ConnectionString_UsernamePasswordPair_Warning;
                    NoticeForm nf = new NoticeForm(warningMsg, Color.Red);
                    nf.Show();
                }
                else
                {
                    connectionString = "Data Source=" + serverName + ";Network Library=DBMSSOCN;Initial Catalog=" + databaseName + ";User ID=" + username + ";Password=" + password + ";";
                }

                result = CommonService.SetupLoginServices.IsServerConnected(connectionString);

                if (result)
                {
                    warningMsg = Properties.Resources.ConnectionString_Success;
                    NoticeForm nf = new NoticeForm(warningMsg, Color.Green);
                    nf.Show();
                }
                else
                {
                    warningMsg = Properties.Resources.ConnectionString_Fail;
                    NoticeForm nf = new NoticeForm(warningMsg, Color.Red);
                    nf.Show();
                }
            }
            BtnTest.Enabled = true;
            BtnNext.Enabled = true;
            BtnCancel.Enabled = true;
            return result;
        }

        void BtnCancel_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
