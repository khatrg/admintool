﻿using System;
using System.Data;
using System.Windows.Forms;

namespace AdminTool
{
    public partial class MainDBList : Form
    {
        public MainDBList()
        {
            InitializeComponent();
            DataTable DBCodeList = new DataTable();
            DBCodeList = CommonService.SetupLoginServices.getDBCodeList();
            AdminDBGrid.AutoGenerateColumns = false;
            AdminDBGrid.Columns[0].DataPropertyName = "BU_CODE";
            AdminDBGrid.Columns[1].DataPropertyName = "DESCR";
            AdminDBGrid.DataSource = DBCodeList;
        }
        string DbCode = "";
        void BtnSelect_Click(object sender, EventArgs e)
        {
            CommonService.SetupLoginServices.SaveMasterDBCode(DbCode);
            ConfirmationForm cf = new ConfirmationForm("DBList", "MasterDB");
            cf.Show();
        }

        void AdminDBGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            DbCode = AdminDBGrid.Rows[e.RowIndex].Cells["DB_CODE"].Value.ToString();
        }
    }
}
