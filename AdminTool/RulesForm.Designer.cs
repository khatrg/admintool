﻿namespace AdminTool
{
    partial class RulesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RulesForm));
            this.ExcGB = new System.Windows.Forms.GroupBox();
            this.DtgvException = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.From = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.To = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ExcToolStrip = new System.Windows.Forms.ToolStrip();
            this.CbxDatabase = new System.Windows.Forms.ToolStripComboBox();
            this.CbxColDB = new System.Windows.Forms.ToolStripComboBox();
            this.TxtFrom = new System.Windows.Forms.ToolStripTextBox();
            this.TxtTo = new System.Windows.Forms.ToolStripTextBox();
            this.BtnAddExc = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.CbxFromDB = new System.Windows.Forms.ToolStripComboBox();
            this.CopyDbx = new System.Windows.Forms.ToolStripDropDownButton();
            this.CopyExcforDB = new System.Windows.Forms.ToolStripMenuItem();
            this.CopyExcForAllDB = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnDeleteException = new System.Windows.Forms.ToolStripButton();
            this.BtnExportException = new System.Windows.Forms.ToolStripButton();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.Columns = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.colToolStrip = new System.Windows.Forms.ToolStrip();
            this.AddAllBtn = new System.Windows.Forms.ToolStripButton();
            this.AddOneBtn = new System.Windows.Forms.ToolStripButton();
            this.RemoveOneBtn = new System.Windows.Forms.ToolStripButton();
            this.RemoveAllBtn = new System.Windows.Forms.ToolStripButton();
            this.lblAvailable = new System.Windows.Forms.Label();
            this.lblExcluded = new System.Windows.Forms.Label();
            this.lbxColAvailable = new System.Windows.Forms.ListBox();
            this.lbxColExcluded = new System.Windows.Forms.ListBox();
            this.grpDBs = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.DtgvDatabase = new System.Windows.Forms.DataGridView();
            this.DBCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DBName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IS_ACTIVE = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dbToolStrip = new System.Windows.Forms.ToolStrip();
            this.BtnExportDB = new System.Windows.Forms.ToolStripButton();
            this.RulesGB = new System.Windows.Forms.GroupBox();
            this.LbxRules = new System.Windows.Forms.ListBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.ExpandHideBtn = new System.Windows.Forms.ToolStripButton();
            this.BtnExportRules = new System.Windows.Forms.ToolStripButton();
            this.miniToolStrip = new System.Windows.Forms.ToolStrip();
            this.ExportRulesBtn = new System.Windows.Forms.ToolStripButton();
            this.ruleToolStrip = new System.Windows.Forms.ToolStrip();
            this.ExcGB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DtgvException)).BeginInit();
            this.ExcToolStrip.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.Columns.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.colToolStrip.SuspendLayout();
            this.grpDBs.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DtgvDatabase)).BeginInit();
            this.dbToolStrip.SuspendLayout();
            this.RulesGB.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.ruleToolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // ExcGB
            // 
            this.ExcGB.Controls.Add(this.DtgvException);
            this.ExcGB.Controls.Add(this.ExcToolStrip);
            this.ExcGB.ForeColor = System.Drawing.Color.Blue;
            this.ExcGB.Location = new System.Drawing.Point(12, 296);
            this.ExcGB.Name = "ExcGB";
            this.ExcGB.Size = new System.Drawing.Size(1063, 283);
            this.ExcGB.TabIndex = 2;
            this.ExcGB.TabStop = false;
            this.ExcGB.Text = "Exceptions for ";
            // 
            // DtgvException
            // 
            this.DtgvException.AllowUserToAddRows = false;
            this.DtgvException.AllowUserToDeleteRows = false;
            this.DtgvException.BackgroundColor = System.Drawing.SystemColors.ScrollBar;
            this.DtgvException.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DtgvException.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.From,
            this.To});
            this.DtgvException.Location = new System.Drawing.Point(0, 40);
            this.DtgvException.Name = "DtgvException";
            this.DtgvException.ReadOnly = true;
            this.DtgvException.Size = new System.Drawing.Size(1051, 233);
            this.DtgvException.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "DB Code";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "DB Name";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 350;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Column";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 350;
            // 
            // From
            // 
            this.From.HeaderText = "From";
            this.From.Name = "From";
            this.From.ReadOnly = true;
            this.From.Width = 104;
            // 
            // To
            // 
            this.To.HeaderText = "To";
            this.To.Name = "To";
            this.To.ReadOnly = true;
            this.To.Width = 104;
            // 
            // ExcToolStrip
            // 
            this.ExcToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CbxDatabase,
            this.CbxColDB,
            this.TxtFrom,
            this.TxtTo,
            this.BtnAddExc,
            this.toolStripSeparator1,
            this.toolStripLabel1,
            this.CbxFromDB,
            this.CopyDbx,
            this.toolStripSeparator2,
            this.BtnDeleteException,
            this.BtnExportException});
            this.ExcToolStrip.Location = new System.Drawing.Point(3, 16);
            this.ExcToolStrip.Name = "ExcToolStrip";
            this.ExcToolStrip.Size = new System.Drawing.Size(1057, 25);
            this.ExcToolStrip.TabIndex = 2;
            this.ExcToolStrip.Text = "ExcToolStrip";
            // 
            // CbxDatabase
            // 
            this.CbxDatabase.Margin = new System.Windows.Forms.Padding(1, 0, 5, 0);
            this.CbxDatabase.Name = "CbxDatabase";
            this.CbxDatabase.Size = new System.Drawing.Size(100, 25);
            this.CbxDatabase.Sorted = true;
            this.CbxDatabase.SelectedIndexChanged += new System.EventHandler(this.CbxDatabase_SelectedIndexChanged);
            // 
            // CbxColDB
            // 
            this.CbxColDB.Margin = new System.Windows.Forms.Padding(1, 0, 5, 0);
            this.CbxColDB.Name = "CbxColDB";
            this.CbxColDB.Size = new System.Drawing.Size(180, 25);
            this.CbxColDB.Sorted = true;
            // 
            // TxtFrom
            // 
            this.TxtFrom.Margin = new System.Windows.Forms.Padding(1, 0, 5, 0);
            this.TxtFrom.Name = "TxtFrom";
            this.TxtFrom.Size = new System.Drawing.Size(100, 25);
            this.TxtFrom.ToolTipText = "From";
            // 
            // TxtTo
            // 
            this.TxtTo.AutoToolTip = true;
            this.TxtTo.Margin = new System.Windows.Forms.Padding(1, 0, 5, 0);
            this.TxtTo.Name = "TxtTo";
            this.TxtTo.Size = new System.Drawing.Size(100, 25);
            this.TxtTo.ToolTipText = "To";
            // 
            // BtnAddExc
            // 
            this.BtnAddExc.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnAddExc.Image = ((System.Drawing.Image)(resources.GetObject("BtnAddExc.Image")));
            this.BtnAddExc.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnAddExc.Margin = new System.Windows.Forms.Padding(0, 1, 5, 2);
            this.BtnAddExc.Name = "BtnAddExc";
            this.BtnAddExc.Size = new System.Drawing.Size(23, 22);
            this.BtnAddExc.Text = "Add";
            this.BtnAddExc.Click += new System.EventHandler(this.BtnAddExc_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Margin = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(69, 22);
            this.toolStripLabel1.Text = "Copy From:";
            // 
            // CbxFromDB
            // 
            this.CbxFromDB.Margin = new System.Windows.Forms.Padding(1, 0, 5, 0);
            this.CbxFromDB.Name = "CbxFromDB";
            this.CbxFromDB.Size = new System.Drawing.Size(100, 25);
            // 
            // CopyDbx
            // 
            this.CopyDbx.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.CopyDbx.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CopyExcforDB,
            this.CopyExcForAllDB});
            this.CopyDbx.Image = ((System.Drawing.Image)(resources.GetObject("CopyDbx.Image")));
            this.CopyDbx.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.CopyDbx.Margin = new System.Windows.Forms.Padding(0, 1, 5, 2);
            this.CopyDbx.Name = "CopyDbx";
            this.CopyDbx.Size = new System.Drawing.Size(29, 22);
            this.CopyDbx.Text = "Copy Exception";
            // 
            // CopyExcforDB
            // 
            this.CopyExcforDB.Name = "CopyExcforDB";
            this.CopyExcforDB.Size = new System.Drawing.Size(243, 22);
            this.CopyExcforDB.Text = "Copy Exception to <DB> Only";
            // 
            // CopyExcForAllDB
            // 
            this.CopyExcForAllDB.Name = "CopyExcForAllDB";
            this.CopyExcForAllDB.Size = new System.Drawing.Size(243, 22);
            this.CopyExcForAllDB.Text = "Copy Exception to All Databases";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Margin = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnDeleteException
            // 
            this.BtnDeleteException.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnDeleteException.Image = ((System.Drawing.Image)(resources.GetObject("BtnDeleteException.Image")));
            this.BtnDeleteException.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnDeleteException.Name = "BtnDeleteException";
            this.BtnDeleteException.Size = new System.Drawing.Size(23, 22);
            this.BtnDeleteException.Text = "Delete";
            this.BtnDeleteException.Click += new System.EventHandler(this.BtnDeleteException_Click);
            // 
            // BtnExportException
            // 
            this.BtnExportException.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.BtnExportException.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnExportException.Image = ((System.Drawing.Image)(resources.GetObject("BtnExportException.Image")));
            this.BtnExportException.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnExportException.Name = "BtnExportException";
            this.BtnExportException.Size = new System.Drawing.Size(23, 22);
            this.BtnExportException.Text = "Export To Excel";
            this.BtnExportException.Click += new System.EventHandler(this.BtnExportException_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 26.99906F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 73.00094F));
            this.tableLayoutPanel1.Controls.Add(this.splitContainer2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.RulesGB, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 5);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1063, 285);
            this.tableLayoutPanel1.TabIndex = 3;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(290, 3);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.Columns);
            this.splitContainer2.Panel1Collapsed = true;
            this.splitContainer2.Panel1MinSize = 350;
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.grpDBs);
            this.splitContainer2.Size = new System.Drawing.Size(770, 279);
            this.splitContainer2.SplitterDistance = 350;
            this.splitContainer2.TabIndex = 4;
            // 
            // Columns
            // 
            this.Columns.Controls.Add(this.tableLayoutPanel3);
            this.Columns.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Columns.ForeColor = System.Drawing.Color.Blue;
            this.Columns.Location = new System.Drawing.Point(0, 0);
            this.Columns.Name = "Columns";
            this.Columns.Size = new System.Drawing.Size(350, 100);
            this.Columns.TabIndex = 1;
            this.Columns.TabStop = false;
            this.Columns.Text = "Columns Excluded from Updates";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 3;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.colToolStrip, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.lblAvailable, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.lblExcluded, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.lbxColAvailable, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.lbxColExcluded, 2, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(344, 81);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // colToolStrip
            // 
            this.colToolStrip.Dock = System.Windows.Forms.DockStyle.Left;
            this.colToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.colToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AddAllBtn,
            this.AddOneBtn,
            this.RemoveOneBtn,
            this.RemoveAllBtn});
            this.colToolStrip.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.VerticalStackWithOverflow;
            this.colToolStrip.Location = new System.Drawing.Point(160, 13);
            this.colToolStrip.Margin = new System.Windows.Forms.Padding(0, 0, 0, 10);
            this.colToolStrip.Name = "colToolStrip";
            this.colToolStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.colToolStrip.Size = new System.Drawing.Size(24, 58);
            this.colToolStrip.TabIndex = 0;
            this.colToolStrip.Text = "toolStrip1";
            // 
            // AddAllBtn
            // 
            this.AddAllBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.AddAllBtn.Image = ((System.Drawing.Image)(resources.GetObject("AddAllBtn.Image")));
            this.AddAllBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.AddAllBtn.Name = "AddAllBtn";
            this.AddAllBtn.Size = new System.Drawing.Size(21, 20);
            this.AddAllBtn.Text = "Add All";
            this.AddAllBtn.Click += new System.EventHandler(this.AddAllBtn_Click);
            // 
            // AddOneBtn
            // 
            this.AddOneBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.AddOneBtn.Image = ((System.Drawing.Image)(resources.GetObject("AddOneBtn.Image")));
            this.AddOneBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.AddOneBtn.Name = "AddOneBtn";
            this.AddOneBtn.Size = new System.Drawing.Size(23, 20);
            this.AddOneBtn.Text = "Add";
            this.AddOneBtn.Click += new System.EventHandler(this.AddOneBtn_Click);
            // 
            // RemoveOneBtn
            // 
            this.RemoveOneBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.RemoveOneBtn.Image = ((System.Drawing.Image)(resources.GetObject("RemoveOneBtn.Image")));
            this.RemoveOneBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.RemoveOneBtn.Name = "RemoveOneBtn";
            this.RemoveOneBtn.Size = new System.Drawing.Size(23, 20);
            this.RemoveOneBtn.Text = "Remove";
            this.RemoveOneBtn.Click += new System.EventHandler(this.RemoveOneBtn_Click);
            // 
            // RemoveAllBtn
            // 
            this.RemoveAllBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.RemoveAllBtn.Image = ((System.Drawing.Image)(resources.GetObject("RemoveAllBtn.Image")));
            this.RemoveAllBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.RemoveAllBtn.Name = "RemoveAllBtn";
            this.RemoveAllBtn.Size = new System.Drawing.Size(23, 20);
            this.RemoveAllBtn.Text = "Remove All";
            this.RemoveAllBtn.Click += new System.EventHandler(this.RemoveAllBtn_Click);
            // 
            // lblAvailable
            // 
            this.lblAvailable.AutoSize = true;
            this.lblAvailable.Location = new System.Drawing.Point(3, 0);
            this.lblAvailable.Name = "lblAvailable";
            this.lblAvailable.Size = new System.Drawing.Size(50, 13);
            this.lblAvailable.TabIndex = 1;
            this.lblAvailable.Text = "Available";
            // 
            // lblExcluded
            // 
            this.lblExcluded.AutoSize = true;
            this.lblExcluded.Location = new System.Drawing.Point(187, 0);
            this.lblExcluded.Name = "lblExcluded";
            this.lblExcluded.Size = new System.Drawing.Size(51, 13);
            this.lblExcluded.TabIndex = 2;
            this.lblExcluded.Text = "Excluded";
            // 
            // lbxColAvailable
            // 
            this.lbxColAvailable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbxColAvailable.FormattingEnabled = true;
            this.lbxColAvailable.Location = new System.Drawing.Point(3, 16);
            this.lbxColAvailable.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.lbxColAvailable.Name = "lbxColAvailable";
            this.lbxColAvailable.Size = new System.Drawing.Size(154, 65);
            this.lbxColAvailable.Sorted = true;
            this.lbxColAvailable.TabIndex = 3;
            // 
            // lbxColExcluded
            // 
            this.lbxColExcluded.DisplayMember = "DESC";
            this.lbxColExcluded.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbxColExcluded.FormattingEnabled = true;
            this.lbxColExcluded.Location = new System.Drawing.Point(187, 16);
            this.lbxColExcluded.Name = "lbxColExcluded";
            this.lbxColExcluded.Size = new System.Drawing.Size(154, 62);
            this.lbxColExcluded.Sorted = true;
            this.lbxColExcluded.TabIndex = 4;
            // 
            // grpDBs
            // 
            this.grpDBs.Controls.Add(this.tableLayoutPanel4);
            this.grpDBs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpDBs.ForeColor = System.Drawing.Color.Blue;
            this.grpDBs.Location = new System.Drawing.Point(0, 0);
            this.grpDBs.Name = "grpDBs";
            this.grpDBs.Size = new System.Drawing.Size(770, 279);
            this.grpDBs.TabIndex = 2;
            this.grpDBs.TabStop = false;
            this.grpDBs.Text = "Databases";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.AutoScroll = true;
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.DtgvDatabase, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.dbToolStrip, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(764, 260);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // DtgvDatabase
            // 
            this.DtgvDatabase.AllowUserToAddRows = false;
            this.DtgvDatabase.AllowUserToDeleteRows = false;
            this.DtgvDatabase.BackgroundColor = System.Drawing.SystemColors.ScrollBar;
            this.DtgvDatabase.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DtgvDatabase.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DBCode,
            this.DBName,
            this.IS_ACTIVE});
            this.DtgvDatabase.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DtgvDatabase.Location = new System.Drawing.Point(3, 28);
            this.DtgvDatabase.Name = "DtgvDatabase";
            this.DtgvDatabase.Size = new System.Drawing.Size(758, 229);
            this.DtgvDatabase.TabIndex = 1;
            this.DtgvDatabase.Tag = "ADMT Databases";
            this.DtgvDatabase.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.DtgvDatabase_DataBindingComplete);
            // 
            // DBCode
            // 
            this.DBCode.Frozen = true;
            this.DBCode.HeaderText = "DB Code";
            this.DBCode.Name = "DBCode";
            // 
            // DBName
            // 
            this.DBName.Frozen = true;
            this.DBName.HeaderText = "DB Name";
            this.DBName.Name = "DBName";
            this.DBName.Width = 515;
            // 
            // IS_ACTIVE
            // 
            this.IS_ACTIVE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.IS_ACTIVE.DataPropertyName = "IS_ACTIVE";
            this.IS_ACTIVE.FalseValue = "False";
            this.IS_ACTIVE.HeaderText = "Active";
            this.IS_ACTIVE.Name = "IS_ACTIVE";
            this.IS_ACTIVE.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.IS_ACTIVE.TrueValue = "True";
            // 
            // dbToolStrip
            // 
            this.dbToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.dbToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BtnExportDB});
            this.dbToolStrip.Location = new System.Drawing.Point(0, 0);
            this.dbToolStrip.Name = "dbToolStrip";
            this.dbToolStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.dbToolStrip.Size = new System.Drawing.Size(764, 25);
            this.dbToolStrip.TabIndex = 0;
            this.dbToolStrip.Text = "toolStrip2";
            // 
            // BtnExportDB
            // 
            this.BtnExportDB.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.BtnExportDB.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnExportDB.Image = ((System.Drawing.Image)(resources.GetObject("BtnExportDB.Image")));
            this.BtnExportDB.ImageTransparentColor = System.Drawing.Color.Black;
            this.BtnExportDB.Name = "BtnExportDB";
            this.BtnExportDB.Size = new System.Drawing.Size(23, 22);
            this.BtnExportDB.Text = "Export To Excel";
            this.BtnExportDB.Click += new System.EventHandler(this.BtnExportDB_Click);
            // 
            // RulesGB
            // 
            this.RulesGB.Controls.Add(this.LbxRules);
            this.RulesGB.Controls.Add(this.toolStrip1);
            this.RulesGB.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RulesGB.ForeColor = System.Drawing.Color.Blue;
            this.RulesGB.Location = new System.Drawing.Point(3, 3);
            this.RulesGB.Name = "RulesGB";
            this.RulesGB.Size = new System.Drawing.Size(281, 279);
            this.RulesGB.TabIndex = 2;
            this.RulesGB.TabStop = false;
            this.RulesGB.Text = "Rules";
            // 
            // LbxRules
            // 
            this.LbxRules.DisplayMember = "DESC";
            this.LbxRules.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LbxRules.FormattingEnabled = true;
            this.LbxRules.ItemHeight = 15;
            this.LbxRules.Location = new System.Drawing.Point(3, 42);
            this.LbxRules.Name = "LbxRules";
            this.LbxRules.Size = new System.Drawing.Size(275, 234);
            this.LbxRules.TabIndex = 2;
            this.LbxRules.Tag = "ADMT Rules";
            this.LbxRules.ValueMember = "ID";
            this.LbxRules.SelectedIndexChanged += new System.EventHandler(this.LbxRules_SelectedIndexChanged);
            // 
            // toolStrip1
            // 
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ExpandHideBtn,
            this.BtnExportRules});
            this.toolStrip1.Location = new System.Drawing.Point(3, 17);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip1.Size = new System.Drawing.Size(275, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // ExpandHideBtn
            // 
            this.ExpandHideBtn.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.ExpandHideBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ExpandHideBtn.Image = ((System.Drawing.Image)(resources.GetObject("ExpandHideBtn.Image")));
            this.ExpandHideBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ExpandHideBtn.Name = "ExpandHideBtn";
            this.ExpandHideBtn.Size = new System.Drawing.Size(23, 22);
            this.ExpandHideBtn.Text = "Expand";
            this.ExpandHideBtn.Click += new System.EventHandler(this.ExpandHideBtn_Click);
            // 
            // BtnExportRules
            // 
            this.BtnExportRules.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.BtnExportRules.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnExportRules.Image = ((System.Drawing.Image)(resources.GetObject("BtnExportRules.Image")));
            this.BtnExportRules.ImageTransparentColor = System.Drawing.Color.Black;
            this.BtnExportRules.Name = "BtnExportRules";
            this.BtnExportRules.Size = new System.Drawing.Size(23, 22);
            this.BtnExportRules.Text = "Export To Excel";
            this.BtnExportRules.ToolTipText = "Export To Excel";
            this.BtnExportRules.Click += new System.EventHandler(this.BtnExportRules_Click);
            // 
            // miniToolStrip
            // 
            this.miniToolStrip.AutoSize = false;
            this.miniToolStrip.CanOverflow = false;
            this.miniToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.miniToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.miniToolStrip.Location = new System.Drawing.Point(0, 3);
            this.miniToolStrip.Name = "miniToolStrip";
            this.miniToolStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.miniToolStrip.Size = new System.Drawing.Size(275, 25);
            this.miniToolStrip.TabIndex = 0;
            // 
            // ExportRulesBtn
            // 
            this.ExportRulesBtn.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.ExportRulesBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ExportRulesBtn.Image = ((System.Drawing.Image)(resources.GetObject("ExportRulesBtn.Image")));
            this.ExportRulesBtn.ImageTransparentColor = System.Drawing.Color.Black;
            this.ExportRulesBtn.Name = "ExportRulesBtn";
            this.ExportRulesBtn.Size = new System.Drawing.Size(23, 22);
            this.ExportRulesBtn.Text = "Export To Excel";
            this.ExportRulesBtn.ToolTipText = "Export To Excel";
            // 
            // ruleToolStrip
            // 
            this.ruleToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.ruleToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ExportRulesBtn});
            this.ruleToolStrip.Location = new System.Drawing.Point(0, 0);
            this.ruleToolStrip.Name = "ruleToolStrip";
            this.ruleToolStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.ruleToolStrip.Size = new System.Drawing.Size(275, 25);
            this.ruleToolStrip.TabIndex = 0;
            this.ruleToolStrip.Text = "toolStrip1";
            // 
            // RulesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1087, 581);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.ExcGB);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "RulesForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Rules Management";
            this.Load += new System.EventHandler(this.RulesForm_Load);
            this.ExcGB.ResumeLayout(false);
            this.ExcGB.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DtgvException)).EndInit();
            this.ExcToolStrip.ResumeLayout(false);
            this.ExcToolStrip.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.Columns.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.colToolStrip.ResumeLayout(false);
            this.colToolStrip.PerformLayout();
            this.grpDBs.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DtgvDatabase)).EndInit();
            this.dbToolStrip.ResumeLayout(false);
            this.dbToolStrip.PerformLayout();
            this.RulesGB.ResumeLayout(false);
            this.RulesGB.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ruleToolStrip.ResumeLayout(false);
            this.ruleToolStrip.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox ExcGB;
        private System.Windows.Forms.ToolStrip ExcToolStrip;
        private System.Windows.Forms.ToolStripComboBox CbxDatabase;
        private System.Windows.Forms.ToolStripComboBox CbxColDB;
        private System.Windows.Forms.ToolStripTextBox TxtFrom;
        private System.Windows.Forms.ToolStripTextBox TxtTo;
        private System.Windows.Forms.ToolStripButton BtnAddExc;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripComboBox CbxFromDB;
        private System.Windows.Forms.ToolStripDropDownButton CopyDbx;
        private System.Windows.Forms.ToolStripMenuItem CopyExcforDB;
        private System.Windows.Forms.ToolStripMenuItem CopyExcForAllDB;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton BtnDeleteException;
        private System.Windows.Forms.DataGridView DtgvException;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn From;
        private System.Windows.Forms.DataGridViewTextBoxColumn To;
        private System.Windows.Forms.ToolStripButton BtnExportException;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.GroupBox Columns;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.ToolStrip colToolStrip;
        private System.Windows.Forms.ToolStripButton AddAllBtn;
        private System.Windows.Forms.ToolStripButton AddOneBtn;
        private System.Windows.Forms.ToolStripButton RemoveOneBtn;
        private System.Windows.Forms.ToolStripButton RemoveAllBtn;
        private System.Windows.Forms.Label lblAvailable;
        private System.Windows.Forms.Label lblExcluded;
        private System.Windows.Forms.ListBox lbxColAvailable;
        private System.Windows.Forms.ListBox lbxColExcluded;
        private System.Windows.Forms.GroupBox grpDBs;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.DataGridView DtgvDatabase;
        private System.Windows.Forms.ToolStrip dbToolStrip;
        private System.Windows.Forms.ToolStripButton BtnExportDB;
        private System.Windows.Forms.ToolStrip miniToolStrip;
        private System.Windows.Forms.ToolStripButton ExportRulesBtn;
        private System.Windows.Forms.ToolStrip ruleToolStrip;
        private System.Windows.Forms.GroupBox RulesGB;
        private System.Windows.Forms.ListBox LbxRules;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton BtnExportRules;
        private System.Windows.Forms.ToolStripButton ExpandHideBtn;
        private System.Windows.Forms.DataGridViewTextBoxColumn DBCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn DBName;
        private System.Windows.Forms.DataGridViewCheckBoxColumn IS_ACTIVE;
    }
}