﻿namespace AdminTool
{
    partial class ClientActivityForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ClientActivityForm));
            this.DtgvRulesLog = new System.Windows.Forms.DataGridView();
            this.EventTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RecordCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ActionDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BtnExport = new System.Windows.Forms.Button();
            this.BtnRefresh = new System.Windows.Forms.Button();
            this.DtpkToDate = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.DtpkFromDate = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.CbxSource = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtDesFilter = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.DtgvRulesLog)).BeginInit();
            this.SuspendLayout();
            // 
            // DtgvRulesLog
            // 
            this.DtgvRulesLog.BackgroundColor = System.Drawing.SystemColors.ScrollBar;
            this.DtgvRulesLog.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DtgvRulesLog.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.EventTime,
            this.RecordCode,
            this.ActionDescription});
            this.DtgvRulesLog.Location = new System.Drawing.Point(12, 160);
            this.DtgvRulesLog.Name = "DtgvRulesLog";
            this.DtgvRulesLog.Size = new System.Drawing.Size(793, 322);
            this.DtgvRulesLog.TabIndex = 25;
            // 
            // EventTime
            // 
            this.EventTime.HeaderText = "Create DateTime";
            this.EventTime.Name = "EventTime";
            this.EventTime.Width = 200;
            // 
            // RecordCode
            // 
            this.RecordCode.HeaderText = "Source";
            this.RecordCode.Name = "RecordCode";
            this.RecordCode.Width = 150;
            // 
            // ActionDescription
            // 
            this.ActionDescription.HeaderText = "Action Description";
            this.ActionDescription.Name = "ActionDescription";
            this.ActionDescription.Width = 400;
            // 
            // BtnExport
            // 
            this.BtnExport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(126)))), ((int)(((byte)(142)))));
            this.BtnExport.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExport.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.BtnExport.Location = new System.Drawing.Point(375, 113);
            this.BtnExport.Name = "BtnExport";
            this.BtnExport.Size = new System.Drawing.Size(75, 31);
            this.BtnExport.TabIndex = 24;
            this.BtnExport.Text = "Export";
            this.BtnExport.UseVisualStyleBackColor = false;
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(126)))), ((int)(((byte)(142)))));
            this.BtnRefresh.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.BtnRefresh.Location = new System.Drawing.Point(259, 113);
            this.BtnRefresh.Name = "BtnRefresh";
            this.BtnRefresh.Size = new System.Drawing.Size(75, 31);
            this.BtnRefresh.TabIndex = 23;
            this.BtnRefresh.Text = "Refresh";
            this.BtnRefresh.UseVisualStyleBackColor = false;
            // 
            // DtpkToDate
            // 
            this.DtpkToDate.CustomFormat = "MM dd yyyy";
            this.DtpkToDate.Location = new System.Drawing.Point(492, 80);
            this.DtpkToDate.Name = "DtpkToDate";
            this.DtpkToDate.ShowCheckBox = true;
            this.DtpkToDate.Size = new System.Drawing.Size(193, 20);
            this.DtpkToDate.TabIndex = 20;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(465, 80);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(21, 15);
            this.label4.TabIndex = 19;
            this.label4.Text = "To";
            // 
            // DtpkFromDate
            // 
            this.DtpkFromDate.CustomFormat = "MM dd yyyy";
            this.DtpkFromDate.Location = new System.Drawing.Point(259, 80);
            this.DtpkFromDate.Name = "DtpkFromDate";
            this.DtpkFromDate.ShowCheckBox = true;
            this.DtpkFromDate.Size = new System.Drawing.Size(191, 20);
            this.DtpkFromDate.TabIndex = 18;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(216, 80);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 15);
            this.label3.TabIndex = 17;
            this.label3.Text = "From";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(153, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 15);
            this.label2.TabIndex = 15;
            this.label2.Text = "Description Filter";
            // 
            // CbxSource
            // 
            this.CbxSource.FormattingEnabled = true;
            this.CbxSource.Location = new System.Drawing.Point(259, 12);
            this.CbxSource.Name = "CbxSource";
            this.CbxSource.Size = new System.Drawing.Size(426, 21);
            this.CbxSource.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(206, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 15);
            this.label1.TabIndex = 13;
            this.label1.Text = "Source";
            // 
            // TxtDesFilter
            // 
            this.TxtDesFilter.Location = new System.Drawing.Point(259, 45);
            this.TxtDesFilter.Name = "TxtDesFilter";
            this.TxtDesFilter.Size = new System.Drawing.Size(426, 20);
            this.TxtDesFilter.TabIndex = 26;
            // 
            // ClientActivityForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(817, 494);
            this.Controls.Add(this.TxtDesFilter);
            this.Controls.Add(this.DtgvRulesLog);
            this.Controls.Add(this.BtnExport);
            this.Controls.Add(this.BtnRefresh);
            this.Controls.Add(this.DtpkToDate);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.DtpkFromDate);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.CbxSource);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "ClientActivityForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Client Activity Log";
            ((System.ComponentModel.ISupportInitialize)(this.DtgvRulesLog)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView DtgvRulesLog;
        private System.Windows.Forms.Button BtnExport;
        private System.Windows.Forms.Button BtnRefresh;
        private System.Windows.Forms.DateTimePicker DtpkToDate;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker DtpkFromDate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox CbxSource;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TxtDesFilter;
        private System.Windows.Forms.DataGridViewTextBoxColumn EventTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn RecordCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn ActionDescription;
    }
}