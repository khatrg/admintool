﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace AdminTool
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            this.IsMdiContainer = true;
        }


        void DatabaseToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            OpenDBForm();
        }

        void RulesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenRulesForm();
        }

        void AuditLogsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenRulesLogForm();
        }

        void ClientAuditLogsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenClientActivityForm();
        }

        void SerializationsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SerializationForm sf = new SerializationForm();
            sf.MdiParent = this;
            sf.Show();
        }

        void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        void CascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        void TileVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        void TileHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        void CloseAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            List<Form> openForms = new List<Form>();

            foreach (Form f in Application.OpenForms)
                openForms.Add(f);

            foreach (Form form in openForms)
            {
                if (form.Name != "MainForm")
                {
                    form.Hide();
                }
            }
        }

        void MainForm_KeyDown(object sender, KeyEventArgs e)
        {
            
            if (e.Control && e.KeyCode == Keys.D)
            {
                OpenDBForm();
            }
            else if (e.Control && e.KeyCode == Keys.R)
            {
                OpenRulesForm();
            }
            else if (e.Control && e.KeyCode == Keys.L)
            {
                OpenRulesLogForm();
            }
            else if (e.Control && e.KeyCode == Keys.M)
            {
                OpenClientActivityForm();
            }else if (e.Control && e.KeyCode == Keys.S)
            {
                SaveAllChanges();
            }
        }

        void OpenRulesForm()
        {
            Form onForm = checkFormOpen("RulesForm");
            if (onForm != null)
            {
                onForm.MdiParent = this;
                onForm.BringToFront();
            }
            else
            {
                RulesForm rf = new RulesForm();
                rf.MdiParent = this;
                rf.Show();
            }
        }

        void OpenDBForm()
        {
            Form onForm = checkFormOpen("DatabaseForm");
            if (onForm != null)
            {
                onForm.MdiParent = this;
                onForm.BringToFront();
            }
            else
            {
                DatabaseForm df = new DatabaseForm();
                df.MdiParent = this;
                df.Show();
            }
        }

        void OpenRulesLogForm()
        {
            Form onForm = checkFormOpen("RulesLogForm");
            if (onForm != null)
            {
                onForm.MdiParent = this;
                onForm.BringToFront();
            }
            else
            {
                RulesLogForm rlf = new RulesLogForm();
                rlf.MdiParent = this;
                rlf.Show();
            }
        }

        void OpenClientActivityForm()
        {
            Form onForm = checkFormOpen("ClientActivityForm");
            if (onForm != null)
            {
                onForm.MdiParent = this;
                onForm.BringToFront();
            }
            else
            {
                ClientActivityForm clf = new ClientActivityForm();
                clf.MdiParent = this;
                clf.Show();
            }
        }

        Form checkFormOpen(string FormName)
        {
            FormCollection fc = Application.OpenForms;
            foreach (Form frm in fc)
            {
                if (frm.Name == FormName)
                {
                    return frm;
                }
            }
            return null;
        }

        void ApplyChangesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveAllChanges();
        }

        void SaveAllChanges()
        {
            
        }
    }
}
