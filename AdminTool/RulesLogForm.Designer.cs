﻿namespace AdminTool
{
    partial class RulesLogForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RulesLogForm));
            this.label1 = new System.Windows.Forms.Label();
            this.CbxAffDB = new System.Windows.Forms.ComboBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.DtpkFromDate = new System.Windows.Forms.DateTimePicker();
            this.DtpkToDate = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.BtnRefresh = new System.Windows.Forms.Button();
            this.BtnExport = new System.Windows.Forms.Button();
            this.DtgvRulesLog = new System.Windows.Forms.DataGridView();
            this.EventType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EventTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RuleName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AffectedDatabase = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RecordCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ActionDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.DtgvRulesLog)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(147, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Affected Database";
            // 
            // CbxAffDB
            // 
            this.CbxAffDB.FormattingEnabled = true;
            this.CbxAffDB.Location = new System.Drawing.Point(259, 12);
            this.CbxAffDB.Name = "CbxAffDB";
            this.CbxAffDB.Size = new System.Drawing.Size(426, 21);
            this.CbxAffDB.TabIndex = 1;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(259, 45);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(426, 21);
            this.comboBox1.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(220, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "Rule";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(216, 80);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 15);
            this.label3.TabIndex = 4;
            this.label3.Text = "From";
            // 
            // DtpkFromDate
            // 
            this.DtpkFromDate.CustomFormat = "MM dd yyyy";
            this.DtpkFromDate.Location = new System.Drawing.Point(259, 80);
            this.DtpkFromDate.Name = "DtpkFromDate";
            this.DtpkFromDate.ShowCheckBox = true;
            this.DtpkFromDate.Size = new System.Drawing.Size(191, 20);
            this.DtpkFromDate.TabIndex = 5;
            // 
            // DtpkToDate
            // 
            this.DtpkToDate.CustomFormat = "MM dd yyyy";
            this.DtpkToDate.Location = new System.Drawing.Point(492, 80);
            this.DtpkToDate.Name = "DtpkToDate";
            this.DtpkToDate.ShowCheckBox = true;
            this.DtpkToDate.Size = new System.Drawing.Size(193, 20);
            this.DtpkToDate.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(465, 80);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(21, 15);
            this.label4.TabIndex = 6;
            this.label4.Text = "To";
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(259, 115);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(191, 21);
            this.comboBox2.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(180, 115);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 15);
            this.label5.TabIndex = 8;
            this.label5.Text = "Event Types";
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(126)))), ((int)(((byte)(142)))));
            this.BtnRefresh.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.BtnRefresh.Location = new System.Drawing.Point(259, 142);
            this.BtnRefresh.Name = "BtnRefresh";
            this.BtnRefresh.Size = new System.Drawing.Size(75, 31);
            this.BtnRefresh.TabIndex = 10;
            this.BtnRefresh.Text = "Refresh";
            this.BtnRefresh.UseVisualStyleBackColor = false;
            // 
            // BtnExport
            // 
            this.BtnExport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(126)))), ((int)(((byte)(142)))));
            this.BtnExport.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExport.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.BtnExport.Location = new System.Drawing.Point(375, 142);
            this.BtnExport.Name = "BtnExport";
            this.BtnExport.Size = new System.Drawing.Size(75, 31);
            this.BtnExport.TabIndex = 11;
            this.BtnExport.Text = "Export";
            this.BtnExport.UseVisualStyleBackColor = false;
            // 
            // DtgvRulesLog
            // 
            this.DtgvRulesLog.BackgroundColor = System.Drawing.SystemColors.ScrollBar;
            this.DtgvRulesLog.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DtgvRulesLog.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.EventType,
            this.EventTime,
            this.RuleName,
            this.AffectedDatabase,
            this.RecordCode,
            this.ActionDescription});
            this.DtgvRulesLog.Location = new System.Drawing.Point(12, 187);
            this.DtgvRulesLog.Name = "DtgvRulesLog";
            this.DtgvRulesLog.Size = new System.Drawing.Size(882, 295);
            this.DtgvRulesLog.TabIndex = 12;
            // 
            // EventType
            // 
            this.EventType.HeaderText = "Type";
            this.EventType.Name = "EventType";
            this.EventType.Width = 70;
            // 
            // EventTime
            // 
            this.EventTime.HeaderText = "Create DateTime";
            this.EventTime.Name = "EventTime";
            this.EventTime.Width = 150;
            // 
            // RuleName
            // 
            this.RuleName.HeaderText = "Rule Name";
            this.RuleName.Name = "RuleName";
            this.RuleName.Width = 150;
            // 
            // AffectedDatabase
            // 
            this.AffectedDatabase.HeaderText = "Affected Database";
            this.AffectedDatabase.Name = "AffectedDatabase";
            // 
            // RecordCode
            // 
            this.RecordCode.HeaderText = "Record Code";
            this.RecordCode.Name = "RecordCode";
            // 
            // ActionDescription
            // 
            this.ActionDescription.HeaderText = "Action Description";
            this.ActionDescription.Name = "ActionDescription";
            this.ActionDescription.Width = 268;
            // 
            // RulesLogForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(906, 494);
            this.Controls.Add(this.DtgvRulesLog);
            this.Controls.Add(this.BtnExport);
            this.Controls.Add(this.BtnRefresh);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.DtpkToDate);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.DtpkFromDate);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.CbxAffDB);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "RulesLogForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Rules Log";
            ((System.ComponentModel.ISupportInitialize)(this.DtgvRulesLog)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox CbxAffDB;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker DtpkFromDate;
        private System.Windows.Forms.DateTimePicker DtpkToDate;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button BtnRefresh;
        private System.Windows.Forms.Button BtnExport;
        private System.Windows.Forms.DataGridView DtgvRulesLog;
        private System.Windows.Forms.DataGridViewTextBoxColumn EventType;
        private System.Windows.Forms.DataGridViewTextBoxColumn EventTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn RuleName;
        private System.Windows.Forms.DataGridViewTextBoxColumn AffectedDatabase;
        private System.Windows.Forms.DataGridViewTextBoxColumn RecordCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn ActionDescription;
    }
}