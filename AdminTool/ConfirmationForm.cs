﻿using System;
using System.Windows.Forms;

namespace AdminTool
{
    public partial class ConfirmationForm : Form
    {
        string FromForm = "";
        string ConfirmType = "";
        public ConfirmationForm(string fromForm, string confirmType)
        {
            InitializeComponent();
            FromForm = fromForm;
            ConfirmType = confirmType;
            if (ConfirmType == "MasterDB")
            {
                LblConfirm.Text = AdminTool.Properties.Resources.Confirmation_MasterDatabase;
            }
        }

        void BtnYes_Click(object sender, EventArgs e)
        {
            if (FromForm == "DBList")
            {
                MainForm mf = new MainForm();
                mf.Show();
                this.Close();
            }
            if (FromForm == "MainForm-Exit")
            {
                Application.Exit();
            }

        }

        void BtnNo_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
