﻿namespace AdminTool
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.MainMenu = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ApplyChangesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ExitAltF4ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CopyDatabaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DiscardChangesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DatabaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RulesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AuditLogsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ClientAuditLogsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.windowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CascadeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TileVerticalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TileHorizontalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CloseAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SerializationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.MasterDBCode = new System.Windows.Forms.ToolStripStatusLabel();
            this.MainMenu.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainMenu
            // 
            this.MainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.manageToolStripMenuItem,
            this.windowToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.MainMenu.Location = new System.Drawing.Point(0, 0);
            this.MainMenu.Name = "MainMenu";
            this.MainMenu.Size = new System.Drawing.Size(1180, 24);
            this.MainMenu.TabIndex = 0;
            this.MainMenu.Text = "menuStrip";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ApplyChangesToolStripMenuItem,
            this.ExitAltF4ToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // ApplyChangesToolStripMenuItem
            // 
            this.ApplyChangesToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("ApplyChangesToolStripMenuItem.Image")));
            this.ApplyChangesToolStripMenuItem.Name = "ApplyChangesToolStripMenuItem";
            this.ApplyChangesToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.ApplyChangesToolStripMenuItem.Text = "Apply Changes     Ctrl+S";
            this.ApplyChangesToolStripMenuItem.Click += new System.EventHandler(this.ApplyChangesToolStripMenuItem_Click);
            // 
            // ExitAltF4ToolStripMenuItem
            // 
            this.ExitAltF4ToolStripMenuItem.Name = "ExitAltF4ToolStripMenuItem";
            this.ExitAltF4ToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.ExitAltF4ToolStripMenuItem.Text = "Exit                          Alt+F4";
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CopyDatabaseToolStripMenuItem,
            this.DiscardChangesToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // CopyDatabaseToolStripMenuItem
            // 
            this.CopyDatabaseToolStripMenuItem.Name = "CopyDatabaseToolStripMenuItem";
            this.CopyDatabaseToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.CopyDatabaseToolStripMenuItem.Text = "Copy Database";
            // 
            // DiscardChangesToolStripMenuItem
            // 
            this.DiscardChangesToolStripMenuItem.Name = "DiscardChangesToolStripMenuItem";
            this.DiscardChangesToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.DiscardChangesToolStripMenuItem.Text = "Discard Changes";
            // 
            // manageToolStripMenuItem
            // 
            this.manageToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.DatabaseToolStripMenuItem,
            this.RulesToolStripMenuItem,
            this.AuditLogsToolStripMenuItem,
            this.ClientAuditLogsToolStripMenuItem});
            this.manageToolStripMenuItem.Name = "manageToolStripMenuItem";
            this.manageToolStripMenuItem.Size = new System.Drawing.Size(62, 20);
            this.manageToolStripMenuItem.Text = "Manage";
            // 
            // DatabaseToolStripMenuItem
            // 
            this.DatabaseToolStripMenuItem.Name = "DatabaseToolStripMenuItem";
            this.DatabaseToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.DatabaseToolStripMenuItem.Text = "Database                    Ctr+D";
            this.DatabaseToolStripMenuItem.Click += new System.EventHandler(this.DatabaseToolStripMenuItem_Click_1);
            // 
            // RulesToolStripMenuItem
            // 
            this.RulesToolStripMenuItem.Name = "RulesToolStripMenuItem";
            this.RulesToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.RulesToolStripMenuItem.Text = "Rules                           Ctrl+R";
            this.RulesToolStripMenuItem.Click += new System.EventHandler(this.RulesToolStripMenuItem_Click);
            // 
            // AuditLogsToolStripMenuItem
            // 
            this.AuditLogsToolStripMenuItem.Name = "AuditLogsToolStripMenuItem";
            this.AuditLogsToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.AuditLogsToolStripMenuItem.Text = "Rules Audit Logs       Ctrl+L";
            this.AuditLogsToolStripMenuItem.Click += new System.EventHandler(this.AuditLogsToolStripMenuItem_Click);
            // 
            // ClientAuditLogsToolStripMenuItem
            // 
            this.ClientAuditLogsToolStripMenuItem.Name = "ClientAuditLogsToolStripMenuItem";
            this.ClientAuditLogsToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.ClientAuditLogsToolStripMenuItem.Text = "Client Audit Logs      Ctrl+M ";
            this.ClientAuditLogsToolStripMenuItem.Click += new System.EventHandler(this.ClientAuditLogsToolStripMenuItem_Click);
            // 
            // windowToolStripMenuItem
            // 
            this.windowToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CascadeToolStripMenuItem,
            this.TileVerticalToolStripMenuItem,
            this.TileHorizontalToolStripMenuItem,
            this.CloseAllToolStripMenuItem});
            this.windowToolStripMenuItem.Name = "windowToolStripMenuItem";
            this.windowToolStripMenuItem.Size = new System.Drawing.Size(68, 20);
            this.windowToolStripMenuItem.Text = "Windows";
            // 
            // CascadeToolStripMenuItem
            // 
            this.CascadeToolStripMenuItem.Name = "CascadeToolStripMenuItem";
            this.CascadeToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.CascadeToolStripMenuItem.Text = "Cascade";
            this.CascadeToolStripMenuItem.Click += new System.EventHandler(this.CascadeToolStripMenuItem_Click);
            // 
            // TileVerticalToolStripMenuItem
            // 
            this.TileVerticalToolStripMenuItem.Name = "TileVerticalToolStripMenuItem";
            this.TileVerticalToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.TileVerticalToolStripMenuItem.Text = "Tile Vertical";
            this.TileVerticalToolStripMenuItem.Click += new System.EventHandler(this.TileVerticalToolStripMenuItem_Click);
            // 
            // TileHorizontalToolStripMenuItem
            // 
            this.TileHorizontalToolStripMenuItem.Name = "TileHorizontalToolStripMenuItem";
            this.TileHorizontalToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.TileHorizontalToolStripMenuItem.Text = "Tile Horizontal";
            this.TileHorizontalToolStripMenuItem.Click += new System.EventHandler(this.TileHorizontalToolStripMenuItem_Click);
            // 
            // CloseAllToolStripMenuItem
            // 
            this.CloseAllToolStripMenuItem.Name = "CloseAllToolStripMenuItem";
            this.CloseAllToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.CloseAllToolStripMenuItem.Text = "Close all";
            this.CloseAllToolStripMenuItem.Click += new System.EventHandler(this.CloseAllToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AboutToolStripMenuItem,
            this.SerializationsToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // AboutToolStripMenuItem
            // 
            this.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem";
            this.AboutToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.AboutToolStripMenuItem.Text = "About";
            // 
            // SerializationsToolStripMenuItem
            // 
            this.SerializationsToolStripMenuItem.Name = "SerializationsToolStripMenuItem";
            this.SerializationsToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.SerializationsToolStripMenuItem.Text = "Serialization";
            this.SerializationsToolStripMenuItem.Click += new System.EventHandler(this.SerializationsToolStripMenuItem_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripStatusLabel3,
            this.toolStripStatusLabel2,
            this.MasterDBCode});
            this.statusStrip1.Location = new System.Drawing.Point(0, 679);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1180, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(39, 17);
            this.toolStripStatusLabel1.Text = "Status";
            // 
            // toolStripStatusLabel3
            // 
            this.toolStripStatusLabel3.AutoSize = false;
            this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.Size = new System.Drawing.Size(880, 17);
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(146, 17);
            this.toolStripStatusLabel2.Text = "Administration Database : ";
            // 
            // MasterDBCode
            // 
            this.MasterDBCode.Name = "MasterDBCode";
            this.MasterDBCode.Size = new System.Drawing.Size(92, 17);
            this.MasterDBCode.Text = "DB Code Master";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1180, 701);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.MainMenu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.KeyPreview = true;
            this.MainMenuStrip = this.MainMenu;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Administration Database Management Tool";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyDown);
            this.MainMenu.ResumeLayout(false);
            this.MainMenu.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip MainMenu;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ApplyChangesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem windowToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ExitAltF4ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem CopyDatabaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem DiscardChangesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem DatabaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem RulesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem AuditLogsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ClientAuditLogsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem CascadeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem TileVerticalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem TileHorizontalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem CloseAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem AboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem SerializationsToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel MasterDBCode;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
    }
}