﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AdminTool
{
    public partial class DatabaseForm : Form
    {
        private ToolStrip toolStrip1;
        class ObjectTest
        {
            public int i = 10;
        }
        private CheckBox checkboxHeader = new CheckBox();
        private CheckBox checkboxRule = new CheckBox();
        public DatabaseForm()
        {
            InitializeComponent();
            DataTable DBCodeList = new DataTable();
            DBCodeList = CommonService.SetupLoginServices.getMasterDBCodeList();
            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.Columns[0].DataPropertyName = "BU_CODE";
            dataGridView1.Columns[0].ReadOnly = true;
            dataGridView1.Columns[1].DataPropertyName = "DESCR";
            dataGridView1.Columns[1].ReadOnly = true;
            dataGridView1.ScrollBars = ScrollBars.Vertical;


            //DataGridViewCheckBoxCell chk = new DataGridViewCheckBoxCell();
            //DataGridViewCheckBoxColumn col = new DataGridViewCheckBoxColumn();
            //dataGridView1.Columns.Add(col);
            //Check all
            Rectangle rect = this.dataGridView1.GetCellDisplayRectangle(2, -1, false);
            rect.X = rect.X + dataGridView1.Columns[2].Width + 365;
            rect.Y = rect.Y + 3;
            
            checkboxHeader.Name = "checkboxHeader";
            checkboxHeader.BackColor = Color.White;
            checkboxHeader.Size = new Size(18, 18);
            checkboxHeader.Location = rect.Location;
            checkboxHeader.CheckedChanged += checkboxHeader_CheckedChanged;
            dataGridView1.Controls.Add(checkboxHeader);
            //-Check all
            dataGridView1.DataSource = DBCodeList;


            DataTable ruleCodeTable = new DataTable();
            ruleCodeTable = CommonService.SetupLoginServices.getRuleCodeList();
            dataGridView2.AutoGenerateColumns = false;
            dataGridView2.Columns[0].DataPropertyName = "DESCR";
            dataGridView2.Columns[0].ReadOnly = true;
            dataGridView2.ScrollBars = ScrollBars.Vertical;
            //DataGridViewCheckBoxColumn col2 = new DataGridViewCheckBoxColumn();
            //dataGridView2.Columns.Add(col2);


            //Check all
            Rectangle rects = this.dataGridView2.GetCellDisplayRectangle(1, -1, false);
            rects.X = rects.X + dataGridView2.Columns[1].Width + 352;
            rects.Y = rects.Y + 3;
            checkboxRule.Name = "checkboxHeader";
            checkboxRule.BackColor = Color.White;
            checkboxRule.Size = new Size(18, 18);
            checkboxRule.Location = rects.Location;
            checkboxRule.CheckedChanged += checkboxHeader_CheckedChanged2;
            dataGridView2.Controls.Add(checkboxRule);
            //-Check all


            dataGridView2.DataSource = ruleCodeTable;
            //Exception Dropdown List
            //DBCodeCbx

            DataTable RuleExceptionList = new DataTable();
            RuleExceptionList = CommonService.SetupLoginServices.getRuleExceptionList();
            DBCodeCbx.ComboBox.ValueMember = "RULE_CODE";
            DBCodeCbx.ComboBox.DisplayMember = "DESCR";

            DBCodeCbx.ComboBox.DataSource = RuleExceptionList;

            //DBCbx2 - Copy From
            DataTable CopyFromDropdownList = new DataTable();
            CopyFromDropdownList = CommonService.SetupLoginServices.getMasterDBCodeList();
            DBCbx2.ComboBox.ValueMember = "BU_CODE";
            DBCbx2.ComboBox.DisplayMember = "BU_CODE";
            DBCbx2.ComboBox.DataSource = CopyFromDropdownList;

            //get Column Exception List
            string currentData = DBCodeCbx.ComboBox.SelectedValue.ToString();
            DBCodeCbx.ComboBox.SelectedIndexChanged += OnIndexChanged;

        }
        private void OnIndexChanged(object sender, EventArgs e)
        {
            
            string currentData = DBCodeCbx.ComboBox.SelectedValue.ToString().Replace("\r\n", "");

            DataTable RuleExceptionCol = new DataTable();
            RuleExceptionCol = CommonService.SetupLoginServices.getRuleExceptionColList(currentData);
            ColDBCbx.ComboBox.ValueMember = "COLUMN_CODE";
            ColDBCbx.ComboBox.DisplayMember = "DESCR";
            ColDBCbx.ComboBox.DataSource = RuleExceptionCol;

            
            //MessageBox.Show("Index changed " + currentData);
        }
        //checkboxHeader_CheckedChanged - Databases
        private void checkboxHeader_CheckedChanged(object sender, EventArgs e)
        {
            //if (!CheckState.Unchecked)


            if (checkboxHeader.Checked == true)
            {
                for (int i = 0; i < dataGridView1.Rows.Count; i++)
                {
                    dataGridView1.Rows[i].Cells[2].Value = true;
                }
            }else
            {
                for (int i = 0; i < dataGridView1.Rows.Count; i++)
                {
                    dataGridView1.Rows[i].Cells[2].Value = false;
                }
            }

            
        }

        //checkboxHeader_CheckedChanged - Databases
        private void checkboxHeader_CheckedChanged2(object sender, EventArgs e)
        {
            if (checkboxRule.Checked == true)
            {
                for (int i = 0; i < dataGridView2.Rows.Count; i++)
                {
                    dataGridView2.Rows[i].Cells[1].Value = true;
                }
            }else
            {
                for (int i = 0; i < dataGridView2.Rows.Count; i++)
                {
                    dataGridView2.Rows[i].Cells[1].Value = false;
                }
            }
                
        }


        class Person
        {
            public string Name { get; set; }
            public string Surname { get; set; }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void ColDBCbx_Click(object sender, EventArgs e)
        {

        }

        private void AddBtn_Click(object sender, EventArgs e)
        {
            //add Exception
            
            DataTable dtc = ExcDataGrid.DataSource as DataTable;
            if(dtc == null)
            {
                DataTable dt = new DataTable();
                DataRow newRow = dt.NewRow();
                string currentData = DBCodeCbx.ComboBox.SelectedValue.ToString().Replace("\r\n", "");
                string ExcepCol = ColDBCbx.ComboBox.SelectedValue.ToString().Replace("\r\n", "");
                string Fromwut = FromTbx.Text.ToString().Replace("\r\n", "");
                string towut = ToTbx.Text.ToString().Replace("\r\n", "");

                dt.Columns.Add("rule", typeof(string));
                dt.Columns.Add("column", typeof(string));
                dt.Columns.Add("from", typeof(string));
                dt.Columns.Add("to", typeof(string));

                newRow["rule"] = currentData;
                newRow["column"] = ExcepCol;
                newRow["from"] = Fromwut;
                newRow["to"] = towut;

                dt.Rows.Add(newRow);

                ExcDataGrid.AutoGenerateColumns = false;
                ExcDataGrid.Columns[0].DataPropertyName = "rule";
                ExcDataGrid.Columns[1].DataPropertyName = "column";
                ExcDataGrid.Columns[2].DataPropertyName = "from";
                ExcDataGrid.Columns[3].DataPropertyName = "to";
                ExcDataGrid.DataSource = dt;
            }else
            {
                DataRow newRow = dtc.NewRow();
                string currentData = DBCodeCbx.ComboBox.SelectedValue.ToString().Replace("\r\n", "");
                string ExcepCol = ColDBCbx.ComboBox.SelectedValue.ToString().Replace("\r\n", "");
                string Fromwut = FromTbx.Text.ToString().Replace("\r\n", "");
                string towut = ToTbx.Text.ToString().Replace("\r\n", "");

                newRow["rule"] = currentData;
                newRow["column"] = ExcepCol;
                newRow["from"] = Fromwut;
                newRow["to"] = towut;

                dtc.Rows.Add(newRow);

                ExcDataGrid.AutoGenerateColumns = false;
                ExcDataGrid.Columns[0].DataPropertyName = "rule";
                ExcDataGrid.Columns[1].DataPropertyName = "column";
                ExcDataGrid.Columns[2].DataPropertyName = "from";
                ExcDataGrid.Columns[3].DataPropertyName = "to";
                ExcDataGrid.DataSource = dtc;
            }

            
            
            //dt.Rows.Add(currentData, ExcepCol, Fromwut, towut);

            //dt = CommonService.SetupLoginServices.getExceptionRows(currentData, ExcepCol, Fromwut, towut);
            //

           

        }

        private void DelBtn_Click_1(object sender, EventArgs e)
        {
            foreach (DataGridViewRow item in this.ExcDataGrid.SelectedRows)
            {
                ExcDataGrid.Rows.RemoveAt(item.Index);
            }
        }
    }
}
