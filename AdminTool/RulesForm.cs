﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace AdminTool
{
    public partial class RulesForm : Form
    {
        public RulesForm()
        {
            InitializeComponent();
        }

        void ExpandHideBtn_Click(object sender, EventArgs e)
        {
            splitContainer2.Panel1Collapsed = !splitContainer2.Panel1Collapsed;
            if (splitContainer2.Panel1Collapsed == true)
            {
                this.ExpandHideBtn.Image = AdminTool.Properties.Resources.forward;
                this.ExpandHideBtn.ToolTipText = "Expand";
            }
            else
            {
                this.ExpandHideBtn.Image = AdminTool.Properties.Resources.backward;
                this.ExpandHideBtn.ToolTipText = "Hide";
            }
        }

        DataTable DBCodeList = new DataTable();
        List<KeyValuePair<string, string>> RuleList = new List<KeyValuePair<string, string>>();
        void RulesForm_Load(object sender, EventArgs e)
        {
            //load Rules list
            RuleList = CommonService.RuleManagementServices.getRuleList();
            LbxRules.DataSource = RuleList;
            LbxRules.DisplayMember = "Value";
            LbxRules.ValueMember = "Key";

            //Load Databases list
            DBCodeList = CommonService.SetupLoginServices.getDBCodeList();
            DtgvDatabase.AutoGenerateColumns = false;
            DtgvDatabase.Columns[0].DataPropertyName = "BU_CODE";
            DtgvDatabase.Columns[1].DataPropertyName = "DESCR";
            DtgvDatabase.DataSource = DBCodeList;
            DtgvDatabase.ForeColor = Color.Black;

            

            //set data for DB exception toolstrip combobox
            DataTable DBForCbxDataBase = CommonService.SetupLoginServices.getDBCodeList();
            CbxDatabase.ComboBox.DataSource = DBForCbxDataBase;
            CbxDatabase.ComboBox.DisplayMember = "BU_CODE";
            CbxDatabase.ComboBox.ValueMember = "BU_CODE";

            DataTable DBForCbxCopyFrom = CommonService.SetupLoginServices.getDBCodeList(); 
            CbxFromDB.ComboBox.DataSource = DBForCbxCopyFrom;
            CbxFromDB.ComboBox.DisplayMember = "BU_CODE";
            CbxFromDB.ComboBox.ValueMember = "BU_CODE";

            //add check box to database grid header
            Rectangle rect = this.DtgvDatabase.GetCellDisplayRectangle(2, -1, false);
            rect.X = rect.X + DtgvDatabase.Columns[2].Width - 40;
            rect.Y = rect.Y + 1;
            CheckBox checkboxHeader = new CheckBox();
            checkboxHeader.Name = "checkboxHeader";
            checkboxHeader.BackColor = Color.White;
            checkboxHeader.Size = new Size(18, 18);
            checkboxHeader.Location = rect.Location;
            checkboxHeader.CheckedChanged += new EventHandler(checkboxHeader_CheckedChanged);
            DtgvDatabase.Controls.Add(checkboxHeader);

            //add column show on dtgv Exception
            if(ExceptionTable.Columns.Count == 0)
            {
                ExceptionTable.Columns.Add("DB_CODE");
                ExceptionTable.Columns.Add("DB_NAME");
                ExceptionTable.Columns.Add("COLUMN");
                ExceptionTable.Columns.Add("FROM");
                ExceptionTable.Columns.Add("TO");
            }
        }

        void checkboxHeader_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox checkAll = (CheckBox)sender;
            foreach (DataGridViewRow row in DtgvDatabase.Rows)
            {
                DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)row.Cells[2];
                if (checkAll.Checked)
                {
                    chk.Value = chk.TrueValue;
                }
                else
                {
                    chk.Value = chk.FalseValue;
                }
            }
        }
        void LbxRules_SelectedIndexChanged(object sender, EventArgs e)
        {
            KeyValuePair<string, string> selectedRule = (KeyValuePair<string, string>)LbxRules.SelectedItem;
            string RuleSelected = (string)selectedRule.Key.Trim();//get selected item key from Rule list
            AllColList = CommonService.RuleManagementServices.getColumnRuleList(RuleSelected);
            AvailableColList = AllColList;
            lbxColAvailable.DataSource = AvailableColList;
            lbxColAvailable.DisplayMember = "Value";
            lbxColAvailable.ValueMember = "Key";

            checkedBUList = CommonService.RuleManagementServices.getActiveDBForRule(RuleSelected);

            ExcGB.Text = Properties.Resources.Exceptions_String + (string)selectedRule.Value.Trim();
            LoadExceptionsList();
            DtgvDatabase_DataBindingComplete(sender, null);

            TxtFrom.Text = "";
            TxtTo.Text = "";
        }

        void AddAllBtn_Click(object sender, EventArgs e)
        {
            ExcludeColList = AllColList;
            AvailableColList = new List<KeyValuePair<string, string>>();
            lbxColExcluded.DataSource = ExcludeColList;
            lbxColExcluded.DisplayMember = "Value";
            lbxColExcluded.ValueMember = "Key";
            lbxColAvailable.DataSource = new List<KeyValuePair<string, string>>();
        }
        List<KeyValuePair<string, string>> checkedBUList = new List<KeyValuePair<string, string>>();
        List<KeyValuePair<string, string>> ExcludeColList = new List<KeyValuePair<string, string>>();//Exclude List for edit item
        List<KeyValuePair<string, string>> AvailableColList = new List<KeyValuePair<string, string>>();//Available List for edit item
        List<KeyValuePair<string, string>> AllColList = new List<KeyValuePair<string, string>>();//Full Col Available list
        List<KeyValuePair<string, string>> ExceptionColList = new List<KeyValuePair<string, string>>();//Full Exception Col list
        void AddOneBtn_Click(object sender, EventArgs e)
        {
            if (lbxColAvailable.SelectedItem != null)
            {
                KeyValuePair<string, string> selectedEntry = (KeyValuePair<string, string>)lbxColAvailable.SelectedItem;
                ExcludeColList.Add(selectedEntry);
                lbxColExcluded.DataSource = new List<KeyValuePair<string, string>>();
                lbxColExcluded.DataSource = ExcludeColList;
                lbxColExcluded.DisplayMember = "Value";
                lbxColExcluded.ValueMember = "Key";
                AvailableColList.Remove(new KeyValuePair<string, string>(selectedEntry.Key.ToString(), selectedEntry.Value.ToString()));
                lbxColAvailable.DataSource = new List<KeyValuePair<string, string>>();
                lbxColAvailable.DataSource = AvailableColList;
                lbxColAvailable.DisplayMember = "Value";
                lbxColAvailable.ValueMember = "Key";
            }
        }

        void RemoveAllBtn_Click(object sender, EventArgs e)
        {
            ExcludeColList = new List<KeyValuePair<string, string>>();
            KeyValuePair<string, string> selectedEntry = (KeyValuePair<string, string>)LbxRules.SelectedItem;
            string RuleSelected = (string)selectedEntry.Key.Trim();
            AllColList = CommonService.RuleManagementServices.getColumnRuleList(RuleSelected);
            AvailableColList = AllColList;
            lbxColExcluded.DataSource = new List<KeyValuePair<string, string>>();
            lbxColAvailable.DataSource = new List<KeyValuePair<string, string>>();
            lbxColAvailable.DataSource = AvailableColList;
            lbxColAvailable.DisplayMember = "Value";
            lbxColAvailable.ValueMember = "Key";
        }

        void RemoveOneBtn_Click(object sender, EventArgs e)
        {
            if (lbxColExcluded.SelectedItem != null)
            {
                KeyValuePair<string, string> selectedEntry = (KeyValuePair<string, string>)lbxColExcluded.SelectedItem;
                ExcludeColList.Remove(new KeyValuePair<string, string>(selectedEntry.Key.ToString(), selectedEntry.Value.ToString()));
                lbxColExcluded.DataSource = new List<KeyValuePair<string, string>>();
                lbxColExcluded.DataSource = ExcludeColList;
                lbxColExcluded.DisplayMember = "Value";
                lbxColExcluded.ValueMember = "Key";
                AvailableColList.Add(selectedEntry);
                lbxColAvailable.DataSource = new List<KeyValuePair<string, string>>();
                lbxColAvailable.DataSource = AvailableColList;
                lbxColAvailable.DisplayMember = "Value";
                lbxColAvailable.ValueMember = "Key";
            }
        }


        DataTable ExceptionTable = new DataTable();
        void BtnAddExc_Click(object sender, EventArgs e)
        {
            string From = TxtFrom.Text;
            string To = TxtTo.Text;
            if (From != "" && To != "")
            {
                DataRowView DBView = (DataRowView)CbxDatabase.ComboBox.SelectedItem;
                string DBCode = DBView.Row[0].ToString();
                string DBName = DBView.Row[1].ToString();
                KeyValuePair<string, string> DColView = (KeyValuePair<string, string>)CbxColDB.ComboBox.SelectedItem;
                string ColName = DColView.Value.ToString();


                DataRow newRow = ExceptionTable.NewRow();
                newRow["DB_CODE"] = DBCode;
                newRow["DB_NAME"] = DBName;
                newRow["COLUMN"] = ColName;
                newRow["FROM"] = From;
                newRow["TO"] = To;

                ExceptionTable.Rows.Add(newRow);

                DtgvException.AutoGenerateColumns = false;
                DtgvException.Columns[0].DataPropertyName = "DB_CODE";
                DtgvException.Columns[1].DataPropertyName = "DB_NAME";
                DtgvException.Columns[2].DataPropertyName = "COLUMN";
                DtgvException.Columns[3].DataPropertyName = "FROM";
                DtgvException.Columns[4].DataPropertyName = "TO";
                DtgvException.DataSource = ExceptionTable;
            }
        }

        void BtnDeleteException_Click(object sender, EventArgs e)
        {
            if (DtgvException.SelectedRows.Count > 0)
            {
                foreach (DataGridViewRow dtrv in DtgvException.SelectedRows)
                {
                    DtgvException.Rows.RemoveAt(dtrv.Index);
                }
            }
        }

        void BtnExportRules_Click(object sender, EventArgs e)
        {
            DataTable BURulesTable = CommonService.CommonServices.getActiveBURules();
            bool result = CommonService.CommonServices.ExportBURulesActive(BURulesTable, DBCodeList, RuleList);
            if (result)
            {
                string msg = Properties.Resources.Export_File_Success;
                NoticeForm nf = new NoticeForm(msg, Color.Green);
                nf.Show();
            }else
            {
                string msg = Properties.Resources.Export_File_Fail;
                NoticeForm nf = new NoticeForm(msg, Color.Red);
                nf.Show();
            }
        }

        void BtnExportDB_Click(object sender, EventArgs e)
        {
            if (DtgvDatabase.SelectedRows.Count > 0)
            {
                var selectedRow = DtgvDatabase.SelectedRows[0];
                DataTable RulesActiveTable = CommonService.CommonServices.getActiveRulesForBU(selectedRow.Cells[0].Value.ToString().Trim());
                bool result = CommonService.CommonServices.ExportRulesActive(RulesActiveTable, selectedRow.Cells[0].Value.ToString().Trim());
                if (result)
                {
                    string msg = Properties.Resources.Export_File_Success;
                    NoticeForm nf = new NoticeForm(msg, Color.Green);
                    nf.Show();
                }
                else
                {
                    string msg = Properties.Resources.Export_File_Fail;
                    NoticeForm nf = new NoticeForm(msg, Color.Red);
                    nf.Show();
                }
            }
        }

        void BtnExportException_Click(object sender, EventArgs e)
        {
            DataRowView DBView = (DataRowView)CbxDatabase.ComboBox.SelectedItem;
            string DBCode = DBView.Row[0].ToString();
            DataTable ExceptionTable = CommonService.CommonServices.getExceptionsForBU(DBCode);
            bool result = CommonService.CommonServices.ExportExceptions(ExceptionTable, DBCode);
            if (result)
            {
                string msg = Properties.Resources.Export_File_Success;
                NoticeForm nf = new NoticeForm(msg, Color.Green);
                nf.Show();
            }
            else
            {
                string msg = Properties.Resources.Export_File_Fail;
                NoticeForm nf = new NoticeForm(msg, Color.Red);
                nf.Show();
            }
        }

        void CbxDatabase_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadExceptionsList();
        }

        void LoadExceptionsList()
        {
            if(DtgvException.Rows.Count > 0)
            {
                ExceptionTable.Rows.Clear();
                DtgvException.DataSource = ExceptionTable; 
            }
            if (ExceptionTable.Columns.Count == 0)
            {
                ExceptionTable.Columns.Add("DB_CODE");
                ExceptionTable.Columns.Add("DB_NAME");
                ExceptionTable.Columns.Add("COLUMN");
                ExceptionTable.Columns.Add("FROM");
                ExceptionTable.Columns.Add("TO");
            }

            DataRowView SelectedDB = (DataRowView)CbxDatabase.ComboBox.SelectedItem;
            if (SelectedDB != null)
            {
                string DBCode = SelectedDB.Row[0].ToString();
                string DBName = SelectedDB.Row[1].ToString();

                //set text for copy exceptions toolstrip
                CopyExcforDB.Text = Properties.Resources.CopyExceptionToDB + " " + DBCode + " only";

                KeyValuePair<string, string> selectedRule = (KeyValuePair<string, string>)LbxRules.SelectedItem;
                string RuleSelected = (string)selectedRule.Key.Trim();

                //set data for rules exception toolstrip combobox
                ExceptionColList = CommonService.RuleManagementServices.getColumnForExceptionList(RuleSelected);
                CbxColDB.ComboBox.DataSource = ExceptionColList;
                CbxColDB.ComboBox.DisplayMember = "Value";
                CbxColDB.ComboBox.ValueMember = "Key";

                DataTable ExcData = CommonService.CommonServices.getExceptionsForGridView(DBCode, RuleSelected);
                
                foreach (DataRow dtr in ExcData.Rows)
                {
                    DataRow newRow = ExceptionTable.NewRow();
                    newRow["DB_CODE"] = DBCode;
                    newRow["DB_NAME"] = DBName;
                    string ColDescr = "";
                    foreach (KeyValuePair<string, string> key in ExceptionColList)
                    {
                        if (key.Key.ToString().Trim() == dtr["COLUMN_CODE"].ToString().Trim())
                        {
                            ColDescr = key.Value.ToString();
                        }
                    }

                    newRow["COLUMN"] = ColDescr;
                    newRow["FROM"] = dtr["FROM"].ToString();
                    newRow["TO"] = dtr["TO"].ToString();
                    ExceptionTable.Rows.Add(newRow);
                }

                DtgvException.AutoGenerateColumns = false;
                DtgvException.Columns[0].DataPropertyName = "DB_CODE";
                DtgvException.Columns[1].DataPropertyName = "DB_NAME";
                DtgvException.Columns[2].DataPropertyName = "COLUMN";
                DtgvException.Columns[3].DataPropertyName = "FROM";
                DtgvException.Columns[4].DataPropertyName = "TO";
                DtgvException.DataSource = ExceptionTable;
                DtgvException.ForeColor = Color.Black;
            }
        }

        void DtgvDatabase_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            if (DtgvDatabase.Rows.Count > 0)
            {
                foreach (DataGridViewRow dtrv in DtgvDatabase.Rows)
                {
                    foreach (KeyValuePair<string, string> item in checkedBUList)
                    {
                        if (dtrv.Cells[0].Value.ToString() == item.Key.ToString())
                        {
                            DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)dtrv.Cells[2];
                            if (item.Value == "True")
                            {
                                chk.Value = chk.TrueValue;
                            }
                            else
                            {
                                chk.Value = chk.FalseValue;
                            }

                        }
                    }
                }
            }
        }
    }
}
