﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;
using System.Windows.Forms;

namespace AdminTool
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
        }

        //login checking windows authentication library ----
        [System.Runtime.InteropServices.DllImport("advapi32.dll")]
        public static extern bool LogonUser(string userName, string domainName, string password, int LogonType, int LogonProvider, ref IntPtr phToken);
        public bool IsValidateCredentials(string userName, string password, string domain)
        {
            IntPtr tokenHandler = IntPtr.Zero;
            bool isValid = LogonUser(userName, domain, password, 3, 0, ref tokenHandler);
            return isValid;
        }

        //login application
        private void BtnLogin_Click(object sender, EventArgs e)
        {
            bool result = IsValidateCredentials(TxtUsername.Text, TxtPassword.Text, System.Environment.UserDomainName);
            string warningMsg = "";
            if (result)
            {
                string ConnectionStr = Properties.Resources.ConnectionString_AdminDB;
                bool connectFlag = false;
                connectFlag = CommonService.SetupLoginServices.CheckMasterConnectionString(ConnectionStr);
                if (connectFlag)
                {
                    bool masterDBFlag = CommonService.SetupLoginServices.CheckMasterDB(ConnectionStr);
                    if (masterDBFlag)
                    {
                        MainForm mf = new MainForm();
                        mf.Show();
                        this.Hide();
                    }
                    else
                    {
                        MainDBList mdl = new MainDBList();
                        mdl.Show();
                        this.Hide();
                    }
                }
                else
                {
                    SetupConnectionForm sm = new SetupConnectionForm();
                    sm.Show();
                    this.Hide();
                }
            }
            else
            {
                warningMsg = Properties.Resources.Login_Fail;
                NoticeForm nf = new NoticeForm(warningMsg, Color.Red);
                nf.Show();
            }
        }


        //exit application
        private void BtnCancel_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void LoginForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                BtnLogin_Click(sender, e);
            }
        }
    }
}
