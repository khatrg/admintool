﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AdminTool
{
    public partial class NoticeForm : Form
    {
        public NoticeForm(string warningMsg, Color color)
        {
            InitializeComponent();
            LblWarning.Text = warningMsg;
            LblWarning.ForeColor = color;
        }

        private void BtnOK_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}




