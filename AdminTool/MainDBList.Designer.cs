﻿namespace AdminTool
{
    partial class MainDBList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainDBList));
            this.BtnSelect = new System.Windows.Forms.Button();
            this.labelSub = new System.Windows.Forms.Label();
            this.AdminDBGrid = new System.Windows.Forms.DataGridView();
            this.DB_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DB_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.AdminDBGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnSelect
            // 
            this.BtnSelect.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(126)))), ((int)(((byte)(142)))));
            this.BtnSelect.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSelect.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.BtnSelect.Location = new System.Drawing.Point(167, 415);
            this.BtnSelect.Name = "BtnSelect";
            this.BtnSelect.Size = new System.Drawing.Size(100, 35);
            this.BtnSelect.TabIndex = 21;
            this.BtnSelect.Text = "Select";
            this.BtnSelect.UseVisualStyleBackColor = false;
            this.BtnSelect.Click += new System.EventHandler(this.BtnSelect_Click);
            // 
            // labelSub
            // 
            this.labelSub.AutoSize = true;
            this.labelSub.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSub.Location = new System.Drawing.Point(70, 23);
            this.labelSub.Name = "labelSub";
            this.labelSub.Size = new System.Drawing.Size(280, 24);
            this.labelSub.TabIndex = 19;
            this.labelSub.Text = "Please selelect Master Database";
            // 
            // AdminDBGrid
            // 
            this.AdminDBGrid.AllowUserToAddRows = false;
            this.AdminDBGrid.AllowUserToDeleteRows = false;
            this.AdminDBGrid.AllowUserToOrderColumns = true;
            this.AdminDBGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.AdminDBGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DB_CODE,
            this.DB_NAME});
            this.AdminDBGrid.Location = new System.Drawing.Point(31, 60);
            this.AdminDBGrid.Name = "AdminDBGrid";
            this.AdminDBGrid.ReadOnly = true;
            this.AdminDBGrid.Size = new System.Drawing.Size(378, 288);
            this.AdminDBGrid.TabIndex = 22;
            this.AdminDBGrid.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.AdminDBGrid_CellClick);
            // 
            // DB_CODE
            // 
            this.DB_CODE.HeaderText = "DB_CODE";
            this.DB_CODE.Name = "DB_CODE";
            this.DB_CODE.ReadOnly = true;
            // 
            // DB_NAME
            // 
            this.DB_NAME.HeaderText = "DB_NAME";
            this.DB_NAME.Name = "DB_NAME";
            this.DB_NAME.ReadOnly = true;
            this.DB_NAME.Width = 235;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Location = new System.Drawing.Point(60, 359);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(326, 43);
            this.label1.TabIndex = 23;
            this.label1.Text = "These settings will be saved by the application and will not be required for next" +
    " time usage";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(50, 357);
            this.label2.Margin = new System.Windows.Forms.Padding(3, 0, 1, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(10, 18);
            this.label2.TabIndex = 24;
            this.label2.Text = "*";
            // 
            // MainDBList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(440, 462);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.AdminDBGrid);
            this.Controls.Add(this.BtnSelect);
            this.Controls.Add(this.labelSub);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainDBList";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Administration Database Management Tool";
            ((System.ComponentModel.ISupportInitialize)(this.AdminDBGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnSelect;
        private System.Windows.Forms.Label labelSub;
        private System.Windows.Forms.DataGridView AdminDBGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn DB_CODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn DB_NAME;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}

